handleErrors = response => {
  if (!response.ok) {
    throw Error(response.statusText);
  }
  return response;
};
handleApiCalls = (endpoint, type, data, param) => {
  var hdr = localStorage.token;
  fetch(`${endpoint}/${param}`, {
    method: type,
    body: data,
    headers: {
      'Content-Type': 'application/json',
      'x-auth-token': hdr
    }
  })
    .then(handleErrors)
    .then(response => response.json())
    .catch(error => console.log(error));
};

export default handleApiCalls;
