import openSocket from 'socket.io-client';
import endpoint from './endpoint';
import notificationFile from '../assets/plucky.mp3';

const socket = openSocket(`${endpoint}`);

function subscribeToOrder(cb) {
  socket.on('orderReceive', orders => {
    playSound();
    cb(null, orders);
  });
}

function playSound() {
  const audio = new Audio(notificationFile);
  audio.play();
}
export { subscribeToOrder };
