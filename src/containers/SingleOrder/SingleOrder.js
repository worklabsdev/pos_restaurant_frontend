import React from "react";
import { Table, Card, Button } from "antd";

const { Column } = Table;

const SingleOrder = ({ data, changeStatus }) => {
  console.log("getting data ranch driveout ", data);
  const totalUnitPrice = data.items.reduce(
    (acc, current) => acc + current.price,
    0
  );
  const totalQuantity = data.items.reduce(
    (acc, current) => acc + current.quantity,
    0
  );
  const totalPrice = data.items.reduce(
    (acc, current) => acc + current.totalPrice,
    0
  );
  const driveoutCharge = data.driveoutCharge;
  const orderType = data.orderType;
  const btnstyle = {
    background: "transparent",
    border: "1px solid black",
    color: "black"
  };

  const dataSource = [
    {
      key: "1",
      totalUnitPrice: `Kr.${data.items.reduce(
        (acc, current) => acc + current.price,
        0
      )}`,
      totalQuantity: data.items.reduce(
        (acc, current) => acc + current.quantity,
        0
      ),
      total: `Kr.${data.items.reduce(
        (acc, current) => acc + current.totalPrice,
        0
      ) + (orderType == "Drive Out" ? driveoutCharge : 0)}`,
      driveoutTax:
        orderType == "Drive Out"
          ? `Kr. ${driveoutCharge - ((driveoutCharge * 100) / 125).toFixed(2)}`
          : 0,
      driveoutCharge:
        orderType == "Drive Out" ? `Kr. ${(driveoutCharge * 100) / 125}` : 0
    }
  ];

  const columns = [
    // { width: "10%" },
    // { width: "10%" },
    // { width: "10%" },
    {
      title: "Drive Out Tax",
      dataIndex: "driveoutTax",
      width: "10%",
      key: "driveoutTax"
    },
    {
      title: "Drive Out Charge",
      dataIndex: "driveoutCharge",
      width: "15%",
      key: "driveoutCharge"
    },
    {
      title: "Total Unit Price",
      dataIndex: "totalUnitPrice",
      width: "13%",
      key: "totalUnitPrice"
    },
    { width: "10%" },

    {
      title: "Total quantity",
      dataIndex: "totalQuantity",
      width: "15%",
      key: "totalQuantity"
    },

    {
      title: "Total",
      dataIndex: "total",
      width: "13%",
      key: "total"
    }
  ];
  console.log("gg1o", data);

  return (
    <>
      <Card>
        {data.customerId && (
          <Card.Grid>
            <p>
              <strong>OTP: {data.otp}</strong>
            </p>
            <p>
              Name: {`${data.customerId.firstName} ${data.customerId.lastName}`}
            </p>
            <p>Phone No.: {data.customerId.phoneNo}</p>
            <p>Email: {data.customerId.email}</p>
          </Card.Grid>
        )}
        {data.customerId && data.customerId.address && (
          <Card.Grid>
            <p>Address:</p>
            <p>{data.customerId.address.line1}</p>
            <p>{data.customerId.address.line2}</p>
            <p>
              {data.customerId.address.city}, {data.customerId.address.state}
            </p>
            <p>{data.customerId.address.pin}</p>
            <p>{data.customerId.address.country}</p>
          </Card.Grid>
        )}

        <Card.Grid>
          {data.orderStatus === "PENDING" && (
            <Button
              style={btnstyle}
              onClick={() => changeStatus("IN-PROCESS", data._id)}
            >
              Confirm
            </Button>
          )}
          {data.orderStatus === "IN-PROCESS" && (
            <Button
              style={btnstyle}
              onClick={() => changeStatus("DELIVERED", data._id)}
            >
              Deliver
            </Button>
          )}
          {(data.orderStatus === "PENDING" ||
            data.orderStatus === "IN-PROCESS") && (
            <Button
              style={btnstyle}
              onClick={() => changeStatus("CANCEL", data._id)}
            >
              Cancel
            </Button>
          )}
        </Card.Grid>
      </Card>
      <Table dataSource={data.items} pagination={false}>
        <Column title="Product Name" dataIndex="name" key="name" />
        <Column title="Item No." dataIndex="itemNo" key="itemNo" />
        <Column
          title="Unit Price"
          dataIndex="price"
          render={text => `Kr.${text}`}
          key="price"
        />
        <Column title="Instruction" dataIndex="instruction" key="instruction" />
        <Column title="Quantity" dataIndex="quantity" key="quantity" />

        <Column
          title="Total Price"
          dataIndex="totalPrice"
          render={text => `Kr.${text}`}
          key="totalPrice"
        />
      </Table>

      <Table dataSource={dataSource} columns={columns} pagination={false} />
    </>
  );
};

export default SingleOrder;
