import React, { Component } from 'react';
import { connect } from 'react-redux';
import Home from '../Home/Home';
import { Button, Col, Row } from 'antd';
import './AdminDashboard.css';
import calculator from '../../assets/calculator.png';
import borg from '../../assets/borg.png';
import multifood from '../../assets/multifood.png';
import culina from '../../assets/culina.png';
import cheesIcon from '../../assets/coca-cola.png';
import pIcon from '../../assets/p.png';
import eIcon from '../../assets/e.png';
import rIcon from '../../assets/r.png';

const marginBottom = {
  marginBottom: '3.5em'
};

class AdminDashboardComponent extends Component {
  render() {
    return (
      <Home isAdmin={true}>
        <div
          className={
            this.props.isBilling
              ? 'dashboard-billing'
              : 'dashboard-main-wrapper'
          }
        >
          <Row className="m dashboard-flex">
            <Col className="items gutter-row">
              <div className="button-style">
                <Button
                  style={marginBottom}
                  className="drinks button-size-icon"
                  shape="circle"
                >
                  <div className="menu-options white">
                    <img src={calculator} className="product-image  invert-c" />
                  </div>
                  <div className="menu-options details">
                    <span className="menu-options price">Accountant</span>
                  </div>
                </Button>
              </div>
            </Col>
            <Col className="items gutter-row">
              <div className="button-style">
                <Button
                  style={marginBottom}
                  className="burger button-size-icon"
                  shape="circle"
                >
                  <div className="menu-options white">
                    <img src={borg} className="product-image  invert-c " />
                  </div>
                  <div className="menu-options details">
                    <span className="menu-options price">Borg</span>
                  </div>
                </Button>
              </div>
            </Col>
            <Col className="items gutter-row">
              <div className="button-style">
                <Button
                  style={marginBottom}
                  className="hot-dog button-size-icon"
                  shape="circle"
                >
                  <div className="menu-options white">
                    <img src={culina} className="product-image  invert-c" />
                  </div>
                  <div className="menu-options details">
                    <span className="menu-options price">Culina</span>
                  </div>
                </Button>
              </div>
            </Col>

            <Col className="items gutter-row">
              <div className="button-style">
                <Button
                  style={marginBottom}
                  className="ice-cream button-size-icon"
                  shape="circle"
                >
                  <div className="menu-options white">
                    <img src={multifood} className="product-image" />
                  </div>
                  <div className="menu-options details">
                    <span className="menu-options price">Multi Food</span>
                  </div>
                </Button>
              </div>
            </Col>

            <Col className="items gutter-row">
              <div className="button-style">
                <Button
                  style={marginBottom}
                  className="chicken2 button-size-icon"
                  shape="circle"
                >
                  <div className="menu-options white">
                    <img src={pIcon} className="product-image" />
                  </div>
                  <div className="menu-options details">
                    <span className="menu-options price">M-Print</span>
                  </div>
                </Button>
              </div>
            </Col>

            <Col className="items gutter-row">
              <div className="button-style">
                <Button
                  style={marginBottom}
                  className="chees button-size-icon"
                  shape="circle"
                >
                  <div className="menu-options white">
                    <img src={eIcon} className="product-image" />
                  </div>
                  <div className="menu-options details">
                    <span className="menu-options price">Hafslund</span>
                  </div>
                </Button>
              </div>
            </Col>

            <Col className="items gutter-row">
              <div className="button-style">
                <Button
                  style={marginBottom}
                  className="chicken button-size-icon"
                  shape="circle"
                >
                  <div className="menu-options white">
                    <img src={rIcon} className="product-image" />
                  </div>
                  <div className="menu-options details">
                    <span className="menu-options price">Asko</span>
                  </div>
                </Button>
              </div>
            </Col>

            <Col className="items gutter-row">
              <div className="button-style">
                <Button
                  style={marginBottom}
                  className="chees button-size-icon"
                  shape="circle"
                >
                  <div className="menu-options white">
                    <img src={cheesIcon} className="product-image" />
                  </div>
                  <div className="menu-options details">
                    <span className="menu-options price">Coca Cola</span>
                  </div>
                </Button>
              </div>
            </Col>
          </Row>
        </div>
      </Home>
    );
  }
}
const mapStateToProps = state => {
  return {};
};
const AdminDashboard = connect(mapStateToProps)(AdminDashboardComponent);
export default AdminDashboard;
