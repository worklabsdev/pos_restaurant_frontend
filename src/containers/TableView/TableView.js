import React, { Component } from "react";
import Home from "../Home/Home";
import { Button, Col, Row, Input, notification } from "antd";
import "./TableView.css";
import { push } from "react-router-redux";
import { connect } from "react-redux";
import table from "../../assets/dining.png";
import endpoint from "../../helpers/endpoint";

const Search = Input.Search;
class TableViewComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      error: false,
      isLoaded: false,
      orderType: "",
      number: 0
    };
  }
  openNotification = (type, message) => {
    notification[type]({
      message: message
    });
  };

  addTable = (e, tableType) => {
    e.preventDefault();
    var hdr = localStorage.token;
    var data = JSON.stringify({
      number: this.state.number,
      orderType: tableType
    });
    fetch(`${endpoint}/table`, {
      method: "POST",
      body: data,
      headers: {
        "x-auth-token": hdr,
        "Content-Type": "application/json"
      }
    })
      .then(res => res.json())
      .then(
        result => {
          if (result.status === "failure") {
            this.openNotification("error", result.data);
          } else {
            this.openNotification(result.status, result.message);
            localStorage.setItem("table", result.data._id);
            this.props.history.push("/billing");
          }
        },
        // Note: it's important to handle errors here
        // instead of a catch() block so that we don't swallow
        // exceptions from actual bugs in components.
        error => {
          this.setState({
            isLoaded: true,
            error
          });
        }
      );
  };

  openNotification = (type, message) => {
    notification[type]({
      message: message
    });
  };

  handleChange = e => {
    this.setState({ [e.target.name]: e.target.value });
  };

  render() {
    return (
      <Home>
        <div className="content1" style={{ marginTop: "10%", width: "90%" }}>
          {/* <Row style={{ marginTop: 50, marginBottom: 50 }}>
            <Col style={{ margin: 'auto' }}>
              <div style={{ textAlign: 'center' }}>
                <Search
                  className="ant-input-dark"
                  placeholder="ID/ Order No."
                  onSearch={value => console.log(value)}
                  enterButton
                  style={{
                    width: '400px',
                    'max-width': '70%',
                    borderRadius: '4px'
                  }}
                />
                <Button
                  type="submit"
                  style={{
                    background: 'green',
                    color: '#fff9e9',
                    lineHeight: '2px',
                    height: '42px',
                    outline: 'none'
                  }}
                >
                  <b>+</b>
                </Button>
              </div>
            </Col>
          </Row> */}

          <Row className="table-flex" style={{ textAlign: "center" }}>
            <div className="item-1">
              <Button className="table-wrapper shadow-dash" shape="circle">
                <img
                  src={table}
                  className="dining-logo"
                  style={{ marginLeft: "15px" }}
                />
                <span
                  style={{
                    marginLeft: "14px",
                    color: "black",
                    fontWeight: "bold"
                  }}
                >
                  TABLE NO.
                </span>
              </Button>
              <form onSubmit={e => this.addTable(e, "PLU1")}>
                <div style={{ marginTop: 10 }}>
                  <input
                    onChange={this.handleChange}
                    name="number"
                    type="number"
                    min="1"
                    max="30"
                    style={{
                      padding: "10px",
                      marginLeft: "35px",
                      width: 80,
                      background: "#ffa733",
                      height: 35,
                      borderRadius: 4,
                      border: "none",
                      boxShadow: "inset 0 0 3px #000000"
                    }}
                    required
                  />
                </div>
                <Button htmlType="submit" id="add-button">
                  <b>+</b>
                </Button>
              </form>
            </div>
            <div className="item">
              <Button
                onClick={e => this.addTable(e, "No Table")}
                className="table-wrapper shadow-dash"
                shape="circle"
                style={{
                  width: "130px",
                  height: "130px",
                  background: "#FA5D5D",
                  color: "white"
                }}
              >
                <div className="no-container">
                  <img
                    src={table}
                    className="dining-logo no-dining "
                    style={{ marginLeft: "15px" }}
                  />
                  <div className="text-block" />
                  <div className="text-block1" />
                </div>
                <span style={{ marginLeft: "14px", fontWeight: "bold" }}>
                  NO TABLE
                </span>
              </Button>
            </div>
          </Row>
        </div>
      </Home>
    );
  }
  navigate = route => {
    const { dispatch } = this.props;
    dispatch(push(route));
  };
}
const mapStateToProps = state => {
  return {};
};
const TableView = connect(mapStateToProps)(TableViewComponent);
export default TableView;
