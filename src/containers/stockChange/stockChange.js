import React, { Component } from 'react';
import Home from '../Home/Home';
import './stockChange.css';
import { connect } from 'react-redux';
import { push } from 'react-router-redux';
import { Form, Input, Select, Button, Row, Col, notification } from 'antd';
import endpoint from '../../helpers/endpoint';

const FormItem = Form.Item;
const Option = Select.Option;

class stockChangeComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      employees: [],
      categories: [],
      products: [],
      quantity: '',
      stockType: ''
    };
  }

  handleStockChange = value => {
    this.setState({ stockType: value });
  };

  handleCatChange = id => {
    let hdr = localStorage.token;
    fetch(`${endpoint}/stock/product/${id}`, {
      method: 'GET',
      headers: {
        'x-auth-token': hdr
      }
    })
      .then(res => res.json())
      .then(
        result => {
          if (result.status === 'failure') {
            this.openNotification('error', result.data);
          } else {
            let products = result.data.map(product => {
              return { name: product.name, id: product._id };
            });
            this.setState({ products });
          }
        },
        error => {
          this.setState({
            isLoaded: true,
            error
          });
        }
      );
  };

  openNotification = (type, message) => {
    notification[type]({
      message: message
    });
  };

  handleProdChange = id => {
    this.setState({ productId: id });
  };

  handleQuantityChange = e => {
    this.setState({ quantity: e.target.value });
  };

  handleEmployeeChange = id => {
    this.setState({ employeeId: id });
  };

  handleSubmit = e => {
    e.preventDefault();
    let { productId, quantity, stockType } = this.state;
    let data = {
      quantity,
      type: stockType
    };
    if (stockType === 'out') {
      data = {
        ...data,
        id: this.state.employeeId
      };
    }
    data = JSON.stringify(data);
    let hdr = localStorage.token;
    console.log(data);
    fetch(`${endpoint}/stockProduct/${productId}`, {
      method: 'POST',
      body: data,
      headers: {
        'x-auth-token': hdr,
        'Content-Type': 'application/json'
      }
    })
      .then(res => res.json())
      .then(
        result => {
          if (result.status === 'failure') {
            this.openNotification('error', result.message);
          } else {
            this.openNotification('success', result.message);
          }
        },
        error => {
          this.setState({
            isLoaded: true,
            error
          });
        }
      );
  };

  openNotification = (type, message) => {
    notification[type]({
      message: message
    });
  };

  componentDidMount = () => {
    let hdr = localStorage.token;
    fetch(`${endpoint}/reducePageDetails`, {
      method: 'GET',
      headers: {
        'x-auth-token': hdr
      }
    })
      .then(res => res.json())
      .then(
        result => {
          if (result.status === 'failure') {
            this.openNotification('error', result.data);
          } else {
            let categories = result.data.categories.map(category => {
              return { name: category.name, id: category._id };
            });
            let employees = result.data.employee.map(employee => {
              return {
                name: `${employee.firstName} ${employee.lastName}`,
                id: employee.id
              };
            });
            this.setState({ categories, employees });
          }
        },
        error => {
          this.setState({
            isLoaded: true,
            error
          });
        }
      );
  };

  render() {
    console.log(this.state);
    const { categories, products, employees, stockType } = this.state;
    return (
      <Home isAdmin={true}>
        <div className="report-container">
          <span className="item">Stock</span>
          <span className="item">
            <span id="less-visible">HOME / </span>
            Stock In/Out
          </span>
          <span className="item">
            <span id="less-visible">
              <div
                onClick={() => this.navigate('/stock')}
                className="back-button-border"
              >
                <i className="fa fa-arrow-circle-left" aria-hidden="true" />
                <span>Back</span>
              </div>
            </span>
          </span>
        </div>
        <Row type="flex" justify="center">
          <Col span={10}>
            <Form onSubmit={e => this.handleSubmit(e)}>
              <FormItem>
                <Select
                  required
                  showSearch
                  onChange={this.handleStockChange}
                  placeholder="Stock In/Stock Out"
                  optionFilterProp="children"
                >
                  <Option value="in">Stock In</Option>
                  <Option value="out">Stock Out</Option>
                </Select>
              </FormItem>
              <FormItem>
                <Select
                  required
                  showSearch
                  placeholder="Select Category"
                  optionFilterProp="children"
                  filterOption={(input, option) =>
                    option.props.children
                      .toLowerCase()
                      .indexOf(input.toLowerCase()) >= 0
                  }
                >
                  {categories.map(category => {
                    return (
                      <Option
                        key={category.id}
                        value={category.name}
                        onClick={() => this.handleCatChange(category.id)}
                      >
                        {category.name}
                      </Option>
                    );
                  })}
                </Select>
              </FormItem>
              <FormItem>
                <Select
                  required
                  showSearch
                  placeholder="Select Product"
                  optionFilterProp="children"
                  filterOption={(input, option) =>
                    option.props.children
                      .toLowerCase()
                      .indexOf(input.toLowerCase()) >= 0
                  }
                >
                  {products.map(product => {
                    return (
                      <Option
                        key={product.id}
                        value={product.name}
                        onClick={() => this.handleProdChange(product.id)}
                      >
                        {product.name}
                      </Option>
                    );
                  })}
                </Select>
              </FormItem>
              <FormItem>
                <Input
                  type="Number"
                  min="1"
                  placeholder="Quantity"
                  onChange={e => this.handleQuantityChange(e)}
                />
              </FormItem>
              {stockType === 'out' ? (
                <FormItem label="Assign Stock To:">
                  <Select
                    required
                    showSearch
                    placeholder="Select Employee"
                    optionFilterProp="children"
                    filterOption={(input, option) =>
                      option.props.children
                        .toLowerCase()
                        .indexOf(input.toLowerCase()) >= 0
                    }
                  >
                    {employees.map(employee => {
                      return (
                        <Option
                          key={employee.id}
                          value={employee.name}
                          onClick={() => this.handleEmployeeChange(employee.id)}
                        >
                          {employee.name}
                        </Option>
                      );
                    })}
                  </Select>
                </FormItem>
              ) : (
                ''
              )}

              <FormItem>
                <Button type="primary" htmlType="submit">
                  Submit
                </Button>
              </FormItem>
            </Form>
          </Col>
        </Row>
      </Home>
    );
  }
  navigate = route => {
    const { dispatch } = this.props;
    dispatch(push(route));
  };
}
const mapStateToProps = state => {
  return {};
};
const changeStock = connect(mapStateToProps)(stockChangeComponent);
export default changeStock;
