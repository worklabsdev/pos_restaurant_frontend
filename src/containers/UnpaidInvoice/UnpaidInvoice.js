import React, { Component } from "react";
import endpoint from "../../helpers/endpoint";
import { Table, Button, notification } from "antd";
import Home from "../Home/Home";
import SingleInvoice from "../SingleInvoice/SingleInvoice";

class UnpaidInvoice extends Component {
  constructor(props) {
    super(props);

    this.state = {
      error: false,
      isLoaded: false,
      items: [],
      dueItems: [],
      currentPage: "unpaidInvoice"
    };
  }

  componentDidMount = () => {
    this.loadUnpaidInvoices();
  };

  loadUnpaidInvoices = () => {
    this.setState({ isLoaded: false });
    const hdr = localStorage.token;
    fetch(`${endpoint}/unpaidInvoice`, {
      method: "GET",
      headers: {
        "x-auth-token": hdr
      }
    })
      .then(res => res.json())
      .then(
        result => {
          if (result.status === "failure") {
            this.openNotification("error", result.data);
          } else {
            console.log(result);
            let data = result.data.invoice.map(item => ({
              ...item,
              createdDate: new Date(item.created).toLocaleDateString(),
              createdTime: new Date(item.created).toLocaleTimeString(),
              taxPrice: item.taxPrice.toFixed(2),
              isLoaded: true
            }));
            let invoiceData = data.reverse();

            let dueData = result.data.dueDatedInvoice.map(item => ({
              ...item,
              createdDate: new Date(item.created).toLocaleDateString(),
              createdTime: new Date(item.created).toLocaleTimeString(),
              taxPrice: item.taxPrice.toFixed(2),
              isLoaded: true
            }));
            let dueInvoiceData = dueData.reverse();

            this.setState({
              items: invoiceData,
              dueItems: dueInvoiceData,
              branchInfo: result.data.branch,
              isLoaded: true
            });
          }
        },
        error => {
          console.log(error);
          this.setState({
            error,
            isLoaded: true
          });
        }
      );
  };

  openNotification = (type, message) => {
    notification[type]({
      message: message
    });
  };

  showInvoice = invoiceNumber => {
    let index;

    index = this.state.items.findIndex(
      invoice => invoice.invoiceNumber === invoiceNumber
    );

    if (index > -1) {
      this.setState({
        currentPage: "invoice",
        billData: this.state.items[index]
      });
    } else if (index < 0) {
      index = this.state.dueItems.findIndex(
        invoice => invoice.invoiceNumber === invoiceNumber
      );
      this.setState({
        currentPage: "invoice",
        billData: this.state.dueItems[index]
      });
    }
    console.log(index);
  };

  goBackButton = () => {
    this.setState({ currentPage: "unpaidInvoice" });
    this.loadUnpaidInvoices();
  };

  render() {
    const { currentPage, error, isLoaded } = this.state;
    const dataColumns = [
      {
        title: "Invoice No.",
        dataIndex: "invoiceNumber",
        key: "invoiceNumber"
      },
      {
        title: "Date",
        dataIndex: "createdDate",
        key: "createdDate"
      },
      {
        title: "Time",
        dataIndex: "createdTime",
        key: "createdTime"
      },
      {
        title: "Order Type",
        dataIndex: "orderType",
        key: "orderType"
      },
      {
        title: "Total",
        dataIndex: "taxPrice",
        key: "taxPrice",
        render: (text, record) => {
          // return `Kr.${this.branchInfo.driveoutCharge + parseInt(text)}`;
          if (record.orderType === "Drive Out") {
            return `Kr.${this.state.branchInfo.driveoutCharge +
              parseInt(text)}`;
          } else {
            return `Kr.${text}`;
          }
        }
      },
      {
        title: "Invoice",
        key: "_id",
        render: (text, record) => (
          <Button onClick={() => this.showInvoice(record.invoiceNumber)}>
            View
          </Button>
        )
      }
    ];
    if (error) {
      return <div>error</div>;
    } else if (currentPage === "unpaidInvoice") {
      return (
        <Home>
          <div className="report-container">
            <span className="item">Credit Invoices</span>
            <span className="item">
              <span id="less-visible">HOME / </span>
              Unpaid Invoices
            </span>
          </div>
          <Table
            loading={isLoaded ? false : true}
            dataSource={this.state.items}
            columns={dataColumns}
            rowKey={record => record.invoiceNumber}
          />
          <div className="report-container">
            <span className="item">Overdue Credit Invoices</span>
            <span className="item">
              <span id="less-visible">HOME / </span>
              Unpaid Invoices
            </span>
          </div>
          <Table
            loading={isLoaded ? false : true}
            dataSource={this.state.dueItems}
            columns={dataColumns}
            rowKey={record => record.invoiceNumber}
          />
        </Home>
      );
    } else if (currentPage === "invoice") {
      return (
        <Home>
          <div className="report-container">
            <span className="item">Unpaid Invoices</span>
            <span className="item">
              <span id="less-visible">HOME / </span>
              Unpaid Invoices
            </span>
            <span className="item">
              <span id="less-visible">
                <div onClick={this.goBackButton} className="back-button-border">
                  <i className="fa fa-arrow-circle-left" aria-hidden="true" />
                  <span>Back</span>
                </div>
              </span>
            </span>
          </div>
          <SingleInvoice
            data={this.state.billData}
            branch={this.state.branchInfo}
          />
        </Home>
      );
    }
  }
}

export default UnpaidInvoice;
