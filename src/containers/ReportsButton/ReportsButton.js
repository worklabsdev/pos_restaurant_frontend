import React from 'react';

const ReportsButton = props => {
  return (
    <button
      style={{ position: 'relative' }}
      className={props.color + ' ' + props.css}
      onClick={props.clicked}
    >
      {props.title === 'ONLINE ORDERS' && props.orders > 0 && (
        <span
          style={{
            background: 'red',
            borderRadius: 100,
            width: 42,
            height: 42,
            padding: 9,
            color: 'white',
            position: 'absolute',
            top: -5,
            right: -5
          }}
        >
          {props.orders}
        </span>
      )}
      <p className='text-items'>{props.title}</p>
    </button>
  );
};

export default ReportsButton;
