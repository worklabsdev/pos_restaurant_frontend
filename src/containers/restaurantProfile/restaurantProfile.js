import React, { Component } from "react";
import Home from "../Home/Home";
import "./restaurantProfile.css";
import { connect } from "react-redux";
import Modal from "react-modal";
import endpoint from "../../helpers/endpoint";
import Loader from "../Loader/Loader";
import { message, notification } from "antd";

const customStyles = {
  content: {
    top: "50%",
    left: "50%",
    right: "auto",
    bottom: "auto",
    marginRight: "-50%",
    transform: "translate(-50%, -50%)"
  }
};

const props = {
  name: "file",
  action: "//jsonplaceholder.typicode.com/posts/",
  headers: {
    authorization: "authorization-text"
  },
  onChange(info) {
    if (info.file.status !== "uploading") {
      console.log(info.file, info.fileList);
    }
    if (info.file.status === "done") {
      message.success(`${info.file.name} file uploaded successfully`);
    } else if (info.file.status === "error") {
      message.error(`${info.file.name} file upload failed.`);
    }
  }
};

class resProfileComponent extends Component {
  constructor(props) {
    super(props);

    this.state = {
      modalIsOpen: false,
      error: false,
      isLoaded: false,
      isEditable: false,
      items: [],
      name: "",
      email: "",
      website: "",
      phoneNo: "",
      taxNo: "",
      description: "",
      line1: "",
      line2: "",
      city: "",
      pin: "",
      state: "",
      country: "",
      currentPage: "",
      logo: "",
      accountNumber: "",
      resId: ""
    };

    this.openModal = this.openModal.bind(this);
    this.closeModal = this.closeModal.bind(this);
    this.setProfile = this.setProfile.bind(this);
  }

  setProfile = e => {
    e.preventDefault();
    let website = this.state.website;
    const regex = /^https{0,1}:\/\//i;
    if (regex.test(website)) {
      website = website.toLowerCase();
    } else {
      website = `http://${website}`;
    }
    var data = JSON.stringify({
      name: this.state.name,
      phoneNo: this.state.phoneNo,
      taxNo: this.state.taxNo,
      description: this.state.description,
      email: this.state.email,
      line1: this.state.line1,
      line2: this.state.line2,
      city: this.state.city,
      pin: this.state.pin,
      state: this.state.state,
      country: this.state.country,
      website
    });
    var hdr = localStorage.token;
    console.log(data);
    fetch(`${endpoint}/profile`, {
      method: "PUT",
      body: data,
      headers: {
        "x-auth-token": hdr,
        "Content-Type": "application/json"
      }
    })
      .then(res => res.json())
      .then(
        result => {
          console.log(result);
          if (result.status === "failure") {
            this.openNotification("error", result.data);
          }
          //  else {
          //   localStorage.setItem('taccountant', 1);
          //   localStorage.setItem('managecat', 1);
          //   localStorage.setItem('crm', 1);
          //   localStorage.setItem('hrm', 1);
          //   localStorage.setItem('stock', 1);
          //   localStorage.setItem('invoice', 1);
          //   window.location.reload();
          // }
          else {
            this.getResProfile();
            this.setState({ currentPage: "" });
          }
        },
        // Note: it's important to handle errors here
        // instead of a catch() block so that we don't swallow
        // exceptions from actual bugs in components.
        error => {
          this.setState({
            isLoaded: true,
            error
          });
        }
      );
  };

  openNotification = (type, message) => {
    notification[type]({
      message: message
    });
  };

  handleChange = e => {
    this.setState({ [e.target.name]: e.target.value });
    console.log(this.state);
  };

  copy = () => {
    const textarea = document.getElementById("menu-button-code");
    textarea.select();
    document.execCommand("copy");
  };

  handleLogoChange = e => {
    console.log(e.target.files[0]);
    const file = e.target.files[0];
    const formData = new FormData();
    formData.append("logo", file);
    var hdr = localStorage.token;
    console.log(formData);
    fetch(`${endpoint}/profile/logo`, {
      method: "POST",
      body: formData,
      headers: {
        "x-auth-token": hdr
      }
    })
      .then(res => res.json())
      .then(
        result => {
          if (result.status === "failure") {
            this.openNotification("error", result.data);
          } else {
            localStorage.setItem("logo", result.data);
            this.getResProfile();
            this.setState({ currentPage: "edit" });
          }
        },
        // Note: it's important to handle errors here
        // instead of a catch() block so that we don't swallow
        // exceptions from actual bugs in components.
        error => {
          this.setState({
            isLoaded: true,
            error
          });
        }
      );
  };

  openModal() {
    this.setState({ modalIsOpen: true });
  }

  closeModal() {
    this.setState({ modalIsOpen: false });
  }

  componentDidMount = () => {
    this.getResProfile();
  };

  getResProfile = () => {
    var hdr = localStorage.token;
    fetch(`${endpoint}/profile`, {
      method: "GET",
      headers: {
        "x-auth-token": hdr
      }
    })
      .then(res => res.json())
      .then(
        result => {
          console.log(result);
          if (result.status === "failure") {
            this.openNotification("error", result.data);
          } else {
            this.setState({
              isLoaded: true,
              items: result.status === "failure" ? [] : result.data,
              name: result.data.name,
              phoneNo: result.data.phoneNo,
              email: result.data.email,
              taxNo: result.data.taxNo,
              description: result.data.description,
              line1: result.data.address.line1,
              line2: result.data.address.line2,
              city: result.data.address.city,
              pin: result.data.address.pin,
              state: result.data.address.state,
              country: result.data.address.country,
              isEditable: result.data.isEditable,
              logo: result.data.logo,
              accountNumber: result.data.accountNumber,
              website: result.data.website,
              resId: result.data._id
            });
            localStorage.setItem("website", result.data.website);
          }
        },
        // Note: it's important to handle errors here
        // instead of a catch() block so that we don't swallow
        // exceptions from actual bugs in components.
        error => {
          this.setState({
            isLoaded: true,
            error
          });
        }
      );
  };

  openNotification = (type, message) => {
    notification[type]({
      message: message
    });
  };

  editResProfile = () => {
    this.setState({ currentPage: "edit" });
  };

  render() {
    const {
      error,
      isLoaded,
      items,
      currentPage,
      name,
      email,
      taxNo,
      phoneNo,
      line1,
      line2,
      city,
      pin,
      state,
      country,
      description,
      isEditable,
      imageUrl,
      logo,
      accountNumber,
      website
    } = this.state;
    console.log(this.state);
    if (!isLoaded) {
      return (
        <Home>
          <Loader />
        </Home>
      );
    } else if (error) {
      return <div>Error: {error.message}</div>;
    } else if (items.length === 0 || currentPage === "edit") {
      return (
        <Home>
          <div className="res">
            <form onSubmit={this.setProfile} className="form1 add-restaurant">
              <li>
                <center>
                  <h2>Add Restaurant Details</h2>
                </center>
              </li>
              <li
                style={{
                  textAlign: "center"
                }}
              >
                <img
                  src={`${endpoint}/${logo}`}
                  style={{ maxWidth: "100px", height: "auto" }}
                />
              </li>
              <li>
                <input
                  className="file-input"
                  type="file"
                  name="icon"
                  accept="image/*"
                  onChange={this.handleLogoChange}
                />
              </li>
              <li>
                <input
                  type="text"
                  className="input1"
                  onChange={this.handleChange}
                  name="name"
                  placeholder="Restaurant Name"
                  value={name}
                  required
                />
              </li>
              <li>
                <input
                  type="email"
                  className="input1"
                  onChange={this.handleChange}
                  name="email"
                  placeholder="E-mail"
                  value={email}
                  required
                />
              </li>
              <li>
                <input
                  type="text"
                  className="input1"
                  onChange={this.handleChange}
                  name="website"
                  placeholder="Website"
                  value={website}
                  required
                />
              </li>
              <li>
                <input
                  type="number"
                  className="input1"
                  onChange={this.handleChange}
                  name="taxNo"
                  placeholder="Tax No"
                  value={taxNo}
                  required
                />
              </li>
              <li>
                <input
                  type="number"
                  onChange={this.handleChange}
                  className="input1"
                  name="phoneNo"
                  min="0000000000"
                  max="9999999999"
                  placeholder="Phone No"
                  value={phoneNo}
                  required
                />
              </li>
              <p style={{ textAlign: "left" }}>Address</p>
              <li>
                <input
                  type="text"
                  onChange={this.handleChange}
                  name="line1"
                  maxlength="20"
                  className="input2"
                  placeholder="Address Line 1"
                  value={line1}
                />
              </li>
              <li>
                <input
                  type="text"
                  onChange={this.handleChange}
                  name="line2"
                  maxlength="20"
                  className="input2"
                  placeholder="Address Line 2"
                  value={line2}
                />
              </li>
              <li style={{ "margin-top": "1em" }}>
                <input
                  style={{
                    maxWidth: "37%",
                    float: "left",
                    margin: "0 2% 0 11%"
                  }}
                  type="text"
                  onChange={this.handleChange}
                  name="city"
                  pattern="[A-Za-z]+"
                  maxlength="20"
                  className="input2"
                  placeholder="City"
                  value={city}
                />

                <input
                  style={{
                    maxWidth: "37%",
                    float: "left",
                    margin: "0 11% 0 2%"
                  }}
                  type="number"
                  onChange={this.handleChange}
                  name="pin"
                  className="input2"
                  placeholder="PIN"
                  value={pin}
                />
              </li>
              <li>
                <input
                  style={{
                    maxWidth: "37%",
                    float: "left",
                    margin: "0 2% 0 11%"
                  }}
                  type="text"
                  onChange={this.handleChange}
                  name="state"
                  pattern="[A-Za-z]+"
                  maxlength="20"
                  className="input2"
                  placeholder="State"
                  value={state}
                />

                <input
                  style={{
                    maxWidth: "37%",
                    float: "left",
                    margin: "0 11% 0 2%"
                  }}
                  type="text"
                  onChange={this.handleChange}
                  name="country"
                  pattern="[A-Za-z]+"
                  maxlength="20"
                  className="input2"
                  placeholder="Country"
                  value={country}
                />
              </li>
              <li>
                <textarea
                  type="text"
                  className="input1"
                  onChange={this.handleChange}
                  name="description"
                  rows="5"
                  placeholder="Description"
                  required
                  value={description}
                />
              </li>
              <li>
                <button type="submit" id="submit-landing1">
                  Submit
                </button>
              </li>
            </form>
          </div>
        </Home>
      );
    } else {
      return (
        <Home isProfile={true}>
          <div className="res-profile-wrapper">
            <div className="res-flex title">
              <img
                src={`${endpoint}/${logo}`}
                style={{ maxWidth: "80px", height: "auto" }}
              />
              <h1 id="hotel-name">
                <strong>{items.name}</strong>
              </h1>
              {isEditable ? (
                <i
                  onClick={this.editResProfile}
                  className="fa fa-pencil"
                  aria-hidden="true"
                />
              ) : (
                ""
              )}
            </div>
            <div className="res-flex">
              <div className="info-hotel-mini">
                <p className="hotel-info">
                  <span>
                    <b>Phone:</b>
                  </span>
                  <span>{items.phoneNo}</span>
                </p>
                <p className="hotel-info">
                  <span>
                    <b>Email:</b>
                  </span>
                  <span>{items.email}</span>
                </p>
                <p className="hotel-info">
                  <span>
                    <b>Website:</b>
                  </span>
                  <span>
                    <a href={items.website}>{items.website}</a>
                  </span>
                </p>
                <p className="hotel-info">
                  <span>
                    <b>Tax number:</b>
                  </span>
                  <span>{items.taxNo}</span>
                </p>
                <p className="hotel-info">
                  <span>
                    <b>Account number:</b>
                  </span>
                  <span>{items.accountNumber}</span>
                </p>
                <p className="hotel-info">
                  <span>
                    <b>Address:</b>
                  </span>
                  <span>
                    {items.address.line1}
                    <br />
                    {items.address.line2}
                    <br />
                    {items.address.city}, {items.address.state}
                    <br />
                    {items.address.pin}, {items.address.country}
                  </span>
                </p>
                <span className="hotel-info description">
                  <span>
                    <h4>
                      <b>About Us:</b>
                    </h4>
                  </span>
                  <span>{items.description}</span>
                </span>
                <span className="hotel-info">
                  <span>
                    <b>Button Preview:</b>
                  </span>
                  <a
                    style={{
                      padding: "5px 20px",
                      color: "#0274be",
                      border: "1px solid #0274be",
                      textDecoration: "none",
                      borderRadius: "4px"
                    }}
                    href={`https://web.a-board.world/restaurant/${
                      this.state.resId
                    }`}
                    target="_blank"
                  >
                    Order Now
                  </a>
                </span>
                <span className="hotel-info">
                  <span>
                    <b>Button Code:</b>
                  </span>
                  <span>
                    <button style={{ color: "#1890ff" }} onClick={this.copy}>
                      Click to Copy
                    </button>
                    <textarea
                      id="menu-button-code"
                      className="small-textarea"
                      rows="4"
                      cols="33"
                      value={`<a style="padding: 5px 20px;color: #0274be; border: 1px solid #0274be;text-decoration: none; border-radius: 4px;" href="https://web.a-board.world/restaurant/${
                        this.state.resId
                      }">Order Now</a>`}
                    />
                  </span>
                </span>
                <span />
              </div>
            </div>
          </div>

          {/* <div className="res-profile-wrapper">
            <div className="res-flex">
              <div>
                <i
                  class="fa fa-pencil"
                  aria-hidden="true"
                  onClick={this.openModal}
                />
                <h1 id="hotel-name">
                  <strong>{items.name}</strong>
                </h1>
                <div className="info-hotel-mini">
                  <p className="hotel-info">
                    <b>Phone:</b> <span>{items.phoneNo}</span>
                  </p>
                  <p className="hotel-info">
                    <b>Email:</b> <span>{items.email}</span>
                  </p>
                  <p className="hotel-info">
                    <b>Tax number:</b> <span>{items.taxNo}</span>
                  </p>
                  <p className="hotel-info">
                    <b>Address:</b> <span>{items.address.line1}</span>
                    <br />
                    <span>{items.address.line2}</span>
                    <br />
                    <span>{items.address.city}, </span>
                    <span>{items.address.state}</span>
                    <br />
                    <span>{items.address.pin}, </span>
                    <span>{items.address.country}</span>
                  </p>
                </div>
              </div>
            </div>

            <div className="lower-flex-profile" />
            <div className="profile-data flex-3">
              <h2 className="profile-data-sub">Description</h2>
              <p className="profile-data-sub">{items.description}</p>
            </div>
          </div> */}
          <Modal
            isOpen={this.state.modalIsOpen}
            onAfterOpen={this.afterOpenModal}
            onRequestClose={this.closeModal}
            style={customStyles}
            contentLabel="Edit Hotel Details"
          >
            <div className="button-container">
              <button onClick={this.closeModal} className="close-button">
                X
              </button>
            </div>
            <form className="add-employee my-modal">
              <li>
                <center>
                  <h2>Edit Hotel Details</h2>
                </center>
              </li>
              <li>
                <input
                  type="text"
                  className="input2"
                  placeholder="Hotel Name"
                  value={items.name}
                />
              </li>
              <li>
                <input
                  type="number"
                  className="input2"
                  placeholder="Contact No."
                  value={items.phoneNo}
                />
              </li>
              <li>
                <input
                  type="text"
                  className="input2"
                  placeholder="E-mail"
                  value={items.email}
                />
              </li>
              <li>
                <input
                  type="number"
                  className="input2"
                  placeholder="Tax no."
                  value={items.taxNo}
                />
              </li>
              <h4>Address</h4>
              <li>
                <textarea className="input2" value={items.address} />
              </li>
              <li>
                <label htmlFor="des">Description</label>
                <textarea
                  className="input2"
                  name="description"
                  id="des"
                  cols="20"
                  rows="5"
                  value={items.description}
                />
              </li>
              <li>
                <button type="submit" id="submit-landing2">
                  Submit
                </button>
              </li>
            </form>
          </Modal>
        </Home>
      );
    }
  }
}
const mapStateToProps = state => {
  return {};
};
const resProfile = connect(mapStateToProps)(resProfileComponent);
export default resProfile;
