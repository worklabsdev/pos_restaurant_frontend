import React, { Component } from "react";
import { push } from "react-router-redux";
import { connect } from "react-redux";
import endpoint from "../../helpers/endpoint";
import { Input, notification } from "antd";

class BillingForm extends Component {
  constructor(props) {
    super(props);

    this.state = {
      product: []
    };
  }

  componentWillReceiveProps = () => {
    this.setState({ product: this.props.product });
  };

  invoiceHandler = e => {
    var taxPrice = this.state.product.reduce(function(acc, x) {
      return acc + x.productPrice * x.quantity;
    }, 0);
    // var subTotal = ((100 * taxPrice) / (100 + this.props.tax)).toFixed(2);
    // var taxPrice = subTotal + (subTotal * this.props.tax) / 100;

    let { type, discount, creditCheck } = this.props;
    let isPaid = creditCheck ? 0 : 1;
    var hdr = localStorage.token;
    var table = localStorage.table;

    var data = JSON.stringify({
      items: this.state.product,
      taxPrice: taxPrice.toFixed(2),
      type,
      isPaid,
      discount
    });
    console.log(data);
    fetch(`${endpoint}/invoice/${table}`, {
      method: "POST",
      body: data,
      headers: {
        "x-auth-token": hdr,
        "Content-Type": "application/json"
      }
    })
      .then(res => res.json())
      .then(result => {
        if (result.status === "failure") {
          this.openNotification(
            "error",
            "Invoice already generated or something went wrong."
          );
        } else {
          this.openNotification("success", result.message);
          localStorage.setItem("table", null);
        }
      })
      .catch(err => {
        console.log(err);
      });
  };

  openNotification = (type, message) => {
    notification[type]({
      message: message
    });
  };

  handleQuantityChange = (e, index) => {
    const value = parseInt(e.target.value);
    console.log("index", index);
    console.log("value", value);
    const quantity = this.state.product[index].quantity;
    console.log(this.state.product);
    const newProduct = this.state.product;
    newProduct[index].quantity = value;
    console.log(newProduct);
    this.setState({ product: newProduct });
  };

  render() {
    console.log("state", this.state);
    const createdOn = new Date(this.props.createdOn);
    const date = createdOn.toLocaleDateString();
    const time = createdOn.toLocaleTimeString();
    const { type, discount, cash } = this.props;
    var taxPrice = this.state.product.reduce(function(acc, x) {
      return acc + x.productPrice * x.quantity;
    }, 0);

    let total = (
      taxPrice - (type === "percent" ? (taxPrice * discount) / 100 : discount)
    ).toFixed(2);
    return (
      <div className="bill">
        <div className="outer">
          <span className="black-line" />
          <span className="white-line" />
          <div className="form-container">
            <div className="form">
              <table className="product-table">
                <tbody id="myBill">
                  {this.state.product.map((x, index) => (
                    <tr key={index}>
                      <td className="product-name" style={{ width: "33%" }}>
                        {x.productName}
                      </td>
                      <td className="single-quantity" style={{ width: "35%" }}>
                        X
                        <Input
                          min="1"
                          type="number"
                          value={this.state.product[index].quantity}
                          onChange={e => this.handleQuantityChange(e, index)}
                          style={{
                            width: 60,
                            display: "inline-block",
                            marginLeft: 10
                          }}
                          /* onChange={e => this.handleQuantity(e, index)} */
                        />
                      </td>
                      <td className="single-price" style={{ width: "25%" }}>
                        Kr. {x.productPrice * x.quantity}
                      </td>
                      <td className="trash-td" style={{ width: "7%" }}>
                        <i
                          className="fa fa-trash"
                          aria-hidden="true"
                          onClick={this.props.delete.bind(this, x.id)}
                        />
                      </td>
                    </tr>
                  ))}
                </tbody>
              </table>
              <table className="table2">
                <tbody>
                  <tr>
                    <td className="time">
                      Created By:
                      <span>{this.props.createdBy}</span>
                    </td>

                    <td className="date">
                      Created On:
                      <br />
                      <span>{`${date} ${time}`}</span>
                    </td>
                  </tr>
                  {/* <tr>
                    <td className="light">Discount</td>
                    <td className="text-right price">0</td>
                  </tr> */}
                  <tr>
                    <td className="sub-total name">SUB-TOTAL</td>
                    <td className="text-right sub-total price">
                      Kr.
                      {(
                        total -
                        (
                          total -
                          ((100 * total) / (100 + this.props.tax)).toFixed(2)
                        ).toFixed(2)
                      ).toFixed(2)}
                    </td>
                  </tr>
                  <tr>
                    <td className="light tax">
                      Discount(
                      {type === "percent" ? `${discount}%` : `Kr.${discount}`})
                    </td>
                    <td className="text-right tax price">
                      - Kr.
                      {type === "percent"
                        ? ((taxPrice * discount) / 100).toFixed(2)
                        : discount.toFixed(2)}
                    </td>
                  </tr>
                  <tr>
                    {this.props.driveoutCharge &&
                    this.props.driveoutCharge > 0 ? (
                      <>
                        <td classBame="tax light">Drive Out Charges</td>
                        <td className="text-right tax price">
                          Kr.{this.props.driveoutCharge}
                        </td>
                      </>
                    ) : (
                      ""
                    )}
                  </tr>
                  <tr>
                    <td className="light tax">
                      VAT(
                      {this.props.tax}
                      %)
                    </td>
                    <td className="text-right tax price">
                      Kr.
                      {(
                        (
                          taxPrice -
                          (type === "percent"
                            ? (taxPrice * discount) / 100
                            : discount)
                        ).toFixed(2) -
                        ((100 * total) / (100 + this.props.tax)).toFixed(2)
                      ).toFixed(2)}
                    </td>
                  </tr>
                  <tr>
                    <td className="total name">TOTAL</td>
                    <td className="text-right total price">
                      Kr.
                      {this.props.driveoutCharge
                        ? Number(total) + Number(this.props.driveoutCharge)
                        : Number(total)}
                    </td>
                  </tr>
                  {cash - total < 0 ? (
                    <tr>
                      <td className="total name">Cash Collected</td>
                      <td className="text-right tax price">
                        Kr.
                        {cash.toFixed(2)}
                      </td>
                    </tr>
                  ) : (
                    <tr>
                      <td className="light tax">Cash to be returned</td>
                      <td className="text-right total price">
                        Kr.
                        {(cash - total).toFixed(2)}
                      </td>
                    </tr>
                  )}
                </tbody>
              </table>
            </div>
          </div>
        </div>
        <div className="bill-button-container">
          <button
            type="button"
            onClick={this.invoiceHandler}
            className="ant-btn leftButton shadow print"
          >
            <b>Print</b>
          </button>
          <button
            onClick={this.props.clicked}
            type="button"
            className="ant-btn leftButton shadow send"
          >
            <b>Save</b>
          </button>
        </div>
      </div>
    );
  }
  navigate = route => {
    const { dispatch } = this.props;
    dispatch(push(route));
  };
}

const mapStateToProps = state => {
  return {};
};
const Billing = connect(mapStateToProps)(BillingForm);

export default BillingForm;
