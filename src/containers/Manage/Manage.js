import React, { Component, Fragment } from "react";
import Home from "../Home/Home";
import {
  Table,
  Input,
  InputNumber,
  Popover,
  Form,
  Button,
  notification,
  Modal,
  Row,
  Col,
  Select
} from "antd";
import endpoint from "../../helpers/endpoint";

const data = [];
const Option = Select.Option;
const FormItem = Form.Item;
const EditableContext = React.createContext();

const EditableRow = ({ form, index, ...props }) => (
  <EditableContext.Provider value={form}>
    <tr {...props} />
  </EditableContext.Provider>
);

const EditableFormRow = Form.create()(EditableRow);

class EditableCell extends Component {
  getInput = () => {
    if (this.props.inputType === "number") {
      return <InputNumber />;
    }
    return <Input />;
  };

  render() {
    const {
      editing,
      dataIndex,
      title,
      inputType,
      record,
      index,
      ...restProps
    } = this.props;

    return (
      <EditableContext.Consumer>
        {form => {
          const { getFieldDecorator } = form;
          return (
            <td {...restProps}>
              {editing ? (
                <FormItem style={{ margin: 0 }}>
                  {getFieldDecorator(dataIndex, {
                    rules: [
                      {
                        required: true,
                        message: `Please Input ${title}!`
                      }
                    ],
                    initialValue: record[dataIndex]
                  })(this.getInput())}
                </FormItem>
              ) : (
                restProps.children
              )}
            </td>
          );
        }}
      </EditableContext.Consumer>
    );
  }
}

class EditableTable extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data,
      editingKey: "",
      isLoaded: false,
      error: false,
      visible: false,
      visiblePopover: false,
      visibleEditPopover: false,
      category: true,
      editingRecipe: false,
      iconModalVisible: false,
      PLU1: false,
      PLU2: false,
      items: [],
      icons: [],
      defaultTagValues: [],
      catId: "",
      newItem: "",
      newPrice: "",
      currentPage: 1,
      itemNo: "",
      productName: "",
      productId: "",
      stockProductId: "",
      recipes: [],
      stockCats: [],
      stockProds: [],

      quantity: "",
      recipeName: "",
      recipeQuantity: "",
      editingRecipeIndex: ""
    };
    this.categoryColumns = [
      {
        title: "Sr. No.",
        width: "8%",
        editable: false,
        render: (text, record, index) =>
          //? for correct sr no., check antd pagination and table/pagination docs
          //? index + (currentpage - 1)*10
          {
            return index + (this.state.currentPage - 1) * 10 + 1;
          }
      },
      {
        title: "Category Name",
        dataIndex: "name",
        width: "18%",
        editable: true,
        render: (text, record, index) => (
          <a
            onClick={() =>
              this.handleClick(
                //? for correct sr no., check antd pagination and table/pagination docs
                //? index + (currentpage - 1)*10
                this.state.items[index + (this.state.currentPage - 1) * 10]._id
              )
            }
          >
            {text}
          </a>
        )
      },
      {
        title: "Tags",
        width: "14%",
        render: (text, record) => {
          const { editingKey } = this.state;
          console.log(record.name);
          console.log(editingKey);
          return editingKey === record.name ? (
            <Select
              mode="tags"
              style={{ width: "100%" }}
              placeholder="Choose Type"
              defaultValue={this.state.defaultTagValues}
              onChange={this.handleChange}
            >
              <Option value="PLU1" key="PLU1">
                PLU1
              </Option>
              <Option value="PLU2" key="PLU2">
                PLU2
              </Option>
            </Select>
          ) : (
            <Fragment>
              <span>{record.catType && record.catType.PLU1 ? "PLU1" : ""}</span>
              {"  "}
              <span>{record.catType && record.catType.PLU2 ? "PLU2" : ""}</span>
            </Fragment>
          );
        }
      },
      {
        title: "Icon",
        dataIndex: "image",
        width: "20%",
        render: (text, record) => {
          return (
            <div
              style={{
                display: "flex",
                justifyContent: "space-evenly",
                alignItems: "center"
              }}
            >
              <img
                src={`${endpoint}/${text}`}
                style={{ maxWidth: 80, maxHeight: 100 }}
              />
              {/* <a
                style={{ color: '#1890ff' }}
                onClick={() => this.editIcon(record)}
              >
                Edit Icon
              </a>
              <a
                style={{ color: '#F44336' }}
                onClick={() => this.deleteIcon(record)}
              >
                Delete Icon
              </a> */}
            </div>
          );
        }
      },
      {
        title: "Edit",
        dataIndex: "operation",
        width: "16%",
        render: (text, record) => {
          const editable = this.isEditing(record);
          return (
            <div>
              {editable ? (
                <span>
                  <EditableContext.Consumer>
                    {form => (
                      <a
                        onClick={() => this.save(form, record.name)}
                        style={{ marginRight: 8 }}
                      >
                        Save
                      </a>
                    )}
                  </EditableContext.Consumer>
                  <Popover
                    content={
                      <span
                        style={{
                          display: "flex",
                          justifyContent: "center"
                        }}
                      >
                        <a
                          style={{ color: "#F44336" }}
                          onClick={() => this.cancel(record.name)}
                        >
                          Cancel
                        </a>
                      </span>
                    }
                    title="Are you Sure?"
                    trigger="click"
                    visible={this.state.visibleEditPopover}
                    onVisibleChange={this.handleEditVisibleChange}
                  >
                    <a
                      style={{ color: "#1890ff" }}
                      onClick={this.openEditPopover}
                    >
                      Cancel
                    </a>
                  </Popover>
                </span>
              ) : (
                <a
                  style={{ color: "#1890ff" }}
                  onClick={() => this.edit(record.name, record)}
                >
                  Edit
                </a>
              )}
            </div>
          );
        }
      },
      {
        title: "Icon",
        width: "12%",
        // key: 'action',
        render: (text, record) => (
          <a
            style={{ color: "#1890ff" }}
            onClick={() => this.editIcon(record._id)}
          >
            Change
          </a>
        )
      },
      {
        title: "Delete",
        width: "12%",
        // key: 'action',
        render: (text, record) => {
          const deletable = this.isDeleting(record);
          return (
            <Fragment>
              {deletable ? (
                <Popover
                  content={
                    <span
                      style={{
                        display: "flex",
                        justifyContent: "space-around"
                      }}
                    >
                      <a style={{ color: "#1890ff" }} onClick={this.hideDelete}>
                        Cancel
                      </a>
                      <a
                        style={{ color: "#F44336" }}
                        onClick={e => this.delete(e, record)}
                      >
                        Delete
                      </a>
                    </span>
                  }
                  title="Are you Sure?"
                  trigger="click"
                  visible={true}
                  onVisibleChange={this.handleVisibleChange}
                >
                  <a
                    onClick={() => this.setState({ popoverId: record.id })}
                    style={{ color: "#F44336" }}
                  >
                    Delete
                  </a>
                </Popover>
              ) : (
                <a
                  onClick={() => this.deleteTemp(record.name)}
                  style={{ color: "#F44336" }}
                >
                  Delete
                </a>
              )}
            </Fragment>
          );
        }
      }
    ];

    this.productColumns = [
      {
        title: "Sr. No.",
        width: "13%",
        editable: false,
        render: (text, record, index) =>
          //? for correct sr no., check antd pagination and table/pagination docs
          //? index + (currentpage - 1)*10
          {
            return index + (this.state.currentPage - 1) * 10 + 1;
          }
      },
      {
        title: "Product Name",
        dataIndex: "name",
        width: "25%",
        editable: true,
        render: (text, record) => {
          // return <a onClick={() => this.showRecipe(record._id)}>{text}</a>;
          return text;
        }
      },
      {
        title: "Product Number",
        dataIndex: "itemNo",
        width: "13%",
        editable: true
      },
      {
        title: "Product Price",
        key: "_id",
        width: "15%",
        editable: true,
        dataIndex: "price",
        render: text => `Kr.${text}`
      },
      {
        title: "Edit",
        dataIndex: "operation",
        width: "20%",
        render: (text, record) => {
          const editable = this.isEditing(record);
          return (
            <div>
              {editable ? (
                <span>
                  <EditableContext.Consumer>
                    {form => (
                      <a
                        onClick={() => this.save(form, record.name)}
                        style={{ marginRight: 8 }}
                      >
                        Save
                      </a>
                    )}
                  </EditableContext.Consumer>
                  <Popover
                    content={
                      <span
                        style={{
                          display: "flex",
                          justifyContent: "center"
                        }}
                      >
                        <a
                          style={{ color: "#F44336" }}
                          onClick={() => this.cancel(record.name)}
                        >
                          Cancel
                        </a>
                      </span>
                    }
                    title="Are you Sure?"
                    trigger="click"
                    visible={this.state.visibleEditPopover}
                    onVisibleChange={this.handleEditVisibleChange}
                  >
                    <a
                      style={{ color: "#1890ff" }}
                      onClick={this.openEditPopover}
                    >
                      Cancel
                    </a>
                  </Popover>
                </span>
              ) : (
                <a
                  style={{ color: "#1890ff" }}
                  onClick={() => this.editProd(record.name)}
                >
                  Edit
                </a>
              )}
            </div>
          );
        }
      },
      {
        title: "Delete",
        width: "14%",
        // key: 'action',
        render: (text, record) => {
          const deletable = this.isDeleting(record);
          return (
            <Fragment>
              {deletable ? (
                <Popover
                  content={
                    <span
                      style={{
                        display: "flex",
                        justifyContent: "space-around"
                      }}
                    >
                      <a style={{ color: "#1890ff" }} onClick={this.hideDelete}>
                        Cancel
                      </a>
                      <a
                        style={{ color: "#F44336" }}
                        onClick={e => this.delete(e, record)}
                      >
                        Delete
                      </a>
                    </span>
                  }
                  title="Are you Sure?"
                  trigger="click"
                  visible={true}
                  onVisibleChange={this.handleVisibleChange}
                >
                  <a
                    onClick={() => this.setState({ popoverId: record.id })}
                    style={{ color: "#F44336" }}
                  >
                    Delete
                  </a>
                </Popover>
              ) : (
                <a
                  onClick={() => this.deleteTemp(record.name)}
                  style={{ color: "#F44336" }}
                >
                  Delete
                </a>
              )}
            </Fragment>
          );
        }
      }
    ];
  }

  componentDidMount() {
    this.categoryPage();
  }

  isEditing = record => {
    return record.name === this.state.editingKey;
  };

  isDeleting = record => {
    return record.name === this.state.deletingKey;
  };

  edit = (key, record) => {
    const defaultTagValues = [];
    if (record.catType && record.catType.PLU1) {
      defaultTagValues.push("PLU1");
    }
    if (record.catType && record.catType.PLU2) {
      defaultTagValues.push("PLU2");
    }

    this.setState({
      editingKey: key,
      visibleEditPopover: false,
      defaultTagValues: defaultTagValues
    });
  };

  editProd = key => {
    this.setState({
      editingKey: key,
      visibleEditPopover: false
    });
  };

  openEditPopover = () => {
    this.setState({ visibleEditPopover: true });
  };

  deleteTemp(key) {
    this.setState({ deletingKey: key });
  }

  save(form, key) {
    form.validateFields((error, row) => {
      if (error) {
        return;
      }
      const newData = [...this.state.items];
      const index = newData.findIndex(item => key === item.name);
      if (index > -1) {
        const item = newData[index];
        newData.splice(index, 1, {
          ...item,
          ...row
        });

        this.setState(
          {
            items: newData,
            editingKey: ""
          },
          this.state.category
            ? this.updateCategory(row.name, item._id)
            : this.updateProduct(row.name, row.price, row.itemNo, item._id)
        );
      } else {
        newData.push(row);
        this.setState({ items: newData, editingKey: "" });
      }
    });
  }

  handleVisibleChange = () => {
    this.setState({ visiblePopover: true });
  };

  hideDelete = () => {
    this.setState({ deletingKey: "" });
  };

  delete = (e, record) => {
    e.preventDefault();
    const newData = [...this.state.items];
    const index = newData.findIndex(item => record._id === item._id);
    if (index > -1) {
      const item = newData[index];
      newData.splice(index, 1);
      this.setState(
        {
          items: newData,
          editingId: item._id
        },
        this.state.category
          ? this.deleteCategory(item._id)
          : this.deleteProduct(item._id)
      );
    } else {
      newData.push(record);
      this.setState({ items: newData, editingKey: "" });
    }
  };

  // delete = (e, record) => {
  //   console.log('teststsetsetsetsete');
  //   e.stopPropagation();
  //   const newData = [...this.state.items];
  //   const index = newData.findIndex(item => record._id === item._id);
  //   if (index > -1) {
  //     const item = newData[index];
  //     newData.splice(index, 1);
  //     this.setState(
  //       {
  //         items: newData,
  //         editingId: item._id
  //       },
  //       this.state.category
  //         ? this.deleteCategory(item._id)
  //         : this.deleteProduct(item._id)
  //     );
  //   } else {
  //     newData.push(record);
  //     this.setState({ items: newData, editingKey: '' });
  //   }
  // };

  cancel = () => {
    this.setState({ editingKey: "" });
  };

  handleOk = e => {
    console.log(e);
    this.setState({
      visible: false,
      stockProductId: ""
    });
  };

  handleCancel = e => {
    console.log(e);
    this.setState({
      visible: false,
      // editingRecipe: true,
      stockProductId: ""
    });
  };

  handleIconModalCancel = e => {
    this.setState({
      iconModalVisible: false
    });
  };

  handleEdit = (index, recipeName, quantity, stockProductId) => {
    console.log(stockProductId);
    this.setState({
      editingRecipe: true,
      editingRecipeIndex: index,
      recipeName,
      quantity,
      stockProductId
    });
  };

  handleRecipeChange = e => {
    this.setState({ [e.target.name]: e.target.value });
  };

  pageChange = pagination => {
    if (pagination) {
      this.setState({ currentPage: pagination.current });
    }
  };

  handleChange = value => {
    console.log(value);
    this.setState({
      PLU1: false,
      PLU2: false
    });
    value.map((item, index) => {
      this.setState({ [item]: true });
    });
  };

  selectIcon = id => {
    let hdr = localStorage.token;
    const { catId } = this.state;
    let data = JSON.stringify({ imageId: id });
    console.log(data);
    fetch(`${endpoint}/category/icon/${catId}`, {
      method: "POST",
      body: data,
      headers: {
        "x-auth-token": hdr,
        "Content-Type": "application/json"
      }
    })
      .then(res => res.json())
      .then(
        result => {
          if (result.status === "failure") {
            console.log(result);
            this.openNotification("error", result.data);
          } else {
            console.log(result);
            this.categoryPage();
            this.openNotification("success", result.message);
            this.setState({ iconModalVisible: false });
          }
        },
        // Note: it's important to handle errors here
        // instead of a catch() block so that we don't swallow
        // exceptions from actual bugs in components.
        error => {
          this.setState({
            isLoaded: true,
            error
          });
        }
      );
  };

  editIcon = id => {
    this.setState({ catId: id });
    this.getCategoryIcon();
  };

  // deleteIcon = record => {
  //   console.log(record);
  //   let hdr = localStorage.token;
  //   const { catId } = this.state;
  //   let data = JSON.stringify({ imageId: '' })
  //   console.log(data);
  //   fetch(`${endpoint}/category/icon/${catId}`, {
  //     method: 'POST',
  //     body: data,
  //     headers: {
  //       'x-auth-token': hdr,
  //       'Content-Type': 'application/json'
  //     }
  //   })
  //     .then(res => res.json())
  //     .then(
  //       result => {
  //         if (result.status === 'failure') {
  //           console.log(result);
  //           this.openNotification('error', result.data);
  //         } else {
  //           console.log(result);
  //           this.categoryPage();
  //           this.openNotification('success', result.message);
  //         }
  //       },
  //       // Note: it's important to handle errors here
  //       // instead of a catch() block so that we don't swallow
  //       // exceptions from actual bugs in components.
  //       error => {
  //         this.setState({
  //           isLoaded: true,
  //           error
  //         });
  //       }
  //     );
  // };

  handleClick = id => {
    if (this.state.category) {
      this.setState({ isLoaded: false });
      this.productPage(id);
    } else {
      console.log("beep bop");
    }
  };

  goToCat = () => {
    this.setState({ category: true, isLoaded: false, items: [] });
    this.categoryPage();
  };

  handleAddCategory = e => {
    e.preventDefault();
    const { newItem } = this.state;
    const newData = {
      // key: count,
      name: newItem
    };

    var hdr = localStorage.token;
    var data = JSON.stringify({
      name: newData.name,
      catType: { PLU1: this.state.PLU1, PLU2: this.state.PLU2 }
    });
    console.log(data);
    fetch(`${endpoint}/category`, {
      method: "POST",
      body: data,
      headers: {
        "x-auth-token": hdr,
        "Content-Type": "application/json"
      }
    })
      .then(res => res.json())
      .then(
        result => {
          if (result.status === "failure") {
            console.log(result);
            this.openNotification("error", result.data);
          } else {
            console.log(result);
            this.setState({ catId: result.data._id });
            this.getCategoryIcon();
            this.categoryPage();
            this.openNotification("success", result.message);
          }
        },
        // Note: it's important to handle errors here
        // instead of a catch() block so that we don't swallow
        // exceptions from actual bugs in components.
        error => {
          this.setState({
            isLoaded: true,
            error
          });
        }
      );
  };

  getCategoryIcon = () => {
    var hdr = localStorage.token;
    fetch(`${endpoint}/icon`, {
      method: "GET",
      headers: {
        "x-auth-token": hdr,
        "Content-Type": "application/json"
      }
    })
      .then(res => res.json())
      .then(
        result => {
          if (result.status === "failure") {
            console.log(result);
          } else {
            this.setState({ icons: result.data });
            this.openIconModal();
          }
        },
        // Note: it's important to handle errors here
        // instead of a catch() block so that we don't swallow
        // exceptions from actual bugs in components.
        error => {
          this.setState({
            isLoaded: true,
            error
          });
        }
      );
  };

  openIconModal = () => {
    this.setState({ iconModalVisible: true });
  };

  handleAddProduct = e => {
    e.preventDefault();
    const { newItem, newPrice, catId, itemNo } = this.state;
    const newData = {
      name: newItem,
      price: newPrice
      //itemNo // uncomment later, backend needs to be changed to accept this.
    };

    var hdr = localStorage.token;
    var data = JSON.stringify({
      name: newData.name,
      price: newData.price,
      itemNo
    });
    console.log(data);
    fetch(`${endpoint}/product/${catId}`, {
      method: "POST",
      body: data,
      headers: {
        "x-auth-token": hdr,
        "Content-Type": "application/json"
      }
    })
      .then(res => res.json())
      .then(
        result => {
          if (result.status === "failure") {
            console.log(result);
            this.openNotification("error", result.data);
          } else {
            console.log(result);
            this.productPage(catId);
            this.openNotification("success", result.message);
          }
        },
        // Note: it's important to handle errors here
        // instead of a catch() block so that we don't swallow
        // exceptions from actual bugs in components.
        error => {
          this.setState({
            isLoaded: true,
            error
          });
        }
      );
  };

  categoryPage = () => {
    var hdr = localStorage.token;
    fetch(`${endpoint}/category`, {
      method: "GET",
      headers: {
        "x-auth-token": hdr
      }
    })
      .then(res => res.json())
      .then(
        result => {
          if (result.status === "failure") {
            this.openNotification("error", result.data);
          } else {
            this.setState({
              isLoaded: true,
              items: result.data.reverse(),
              newItem: "",
              newPrice: ""
            });
          }
        },
        // Note: it's important to handle errors here
        // instead of a catch() block so that we don't swallow
        // exceptions from actual bugs in components.
        error => {
          this.setState({
            isLoaded: true,
            error
          });
        }
      );
  };

  StockCategoryPage = () => {
    var hdr = localStorage.token;
    fetch(`${endpoint}/stock`, {
      method: "GET",
      headers: {
        "x-auth-token": hdr
      }
    })
      .then(res => res.json())
      .then(
        result => {
          if (result.status === "failure") {
            this.openNotification("error", result.data);
          } else {
            console.log(result);
            this.setState({
              isLoaded: true,
              stockCats: result.data
            });
          }
        },
        // Note: it's important to handle errors here
        // instead of a catch() block so that we don't swallow
        // exceptions from actual bugs in components.
        error => {
          this.setState({
            isLoaded: true,
            error
          });
        }
      );
  };

  productPage = id => {
    var hdr = localStorage.token;
    fetch(`${endpoint}/product/${id}`, {
      method: "GET",
      headers: {
        "x-auth-token": hdr
      }
    })
      .then(res => res.json())
      .then(
        result => {
          if (result.status === "failure") {
            this.openNotification("error", result.data);
          } else {
            this.setState({
              isLoaded: true,
              items: result.data.reverse(),
              category: false,
              catId: id,
              newItem: "",
              newPrice: "",
              itemNo: ""
            });
          }
        },
        // Note: it's important to handle errors here
        // instead of a catch() block so that we don't swallow
        // exceptions from actual bugs in components.
        error => {
          this.setState({
            isLoaded: true,
            error
          });
        }
      );
  };

  updateCategory = (name, id) => {
    let hdr = localStorage.token;
    const { PLU1, PLU2 } = this.state;
    let data = JSON.stringify({
      name,
      catType: {
        PLU1,
        PLU2
      }
    });
    console.log(data);
    fetch(`${endpoint}/category/${id}`, {
      method: "PUT",
      body: data,
      headers: {
        "x-auth-token": hdr,
        "Content-Type": "application/json"
      }
    })
      .then(res => res.json())
      .then(result => {
        if (result.status === "failure") {
          this.openNotification("error", result.message);
        } else {
          this.categoryPage();
          this.openNotification("success", "Category updated successfully.");
          console.log(result.message);
        }
      });
  };

  updateProduct = (name, price, itemNo, id) => {
    let hdr = localStorage.token;
    let data = JSON.stringify({ name, price, itemNo });
    fetch(`${endpoint}/op-product/${id}`, {
      method: "PUT",
      body: data,
      headers: {
        "x-auth-token": hdr,
        "Content-Type": "application/json"
      }
    })
      .then(res => res.json())
      .then(result => {
        if (result.status === "failure") {
          this.openNotification("error", "err");
        } else {
          this.productPage(this.state.catId);
          this.openNotification("success", "Product updated successfully.");
        }
      });
  };

  deleteCategory = id => {
    let hdr = localStorage.token;
    fetch(`${endpoint}/category/${id}`, {
      method: "DELETE",
      headers: {
        "x-auth-token": hdr,
        "Content-Type": "application/json"
      }
    })
      .then(res => res.json())
      .then(result => {
        if (result.status === "failure") {
          this.openNotification("error", result.data);
        } else {
          this.openNotification("success", "Category deleted successfully.");
          console.log(result.message);
        }
      });
  };

  deleteProduct = id => {
    let hdr = localStorage.token;
    fetch(`${endpoint}/op-product/${id}`, {
      method: "DELETE",
      headers: {
        "x-auth-token": hdr,
        "Content-Type": "application/json"
      }
    })
      .then(res => res.json())
      .then(result => {
        if (result.status === "failure") {
          this.openNotification("error", result.data);
        } else {
          this.openNotification("success", "Product deleted successfully.");
          console.log(result.message);
        }
      });
  };

  openNotification = (type, message) => {
    notification[type]({
      message: message
    });
  };

  showRecipe = id => {
    var hdr = localStorage.token;
    fetch(`${endpoint}/singleProduct/${id}`, {
      method: "GET",
      headers: {
        "x-auth-token": hdr
      }
    })
      .then(res => res.json())
      .then(
        result => {
          if (result.status === "failure") {
            this.openNotification("error", result.data);
          } else {
            this.setState({
              visible: true,
              recipes: result.data.recipe,
              productId: id
            });
          }
          this.StockCategoryPage();
        },
        // Note: it's important to handle errors here
        // instead of a catch() block so that we don't swallow
        // exceptions from actual bugs in components.
        error => {
          this.setState({
            isLoaded: true,
            error
          });
        }
      );
  };

  handleCatChange = id => {
    let hdr = localStorage.token;
    fetch(`${endpoint}/stock/product/${id}`, {
      method: "GET",
      headers: {
        "x-auth-token": hdr
      }
    })
      .then(res => res.json())
      .then(
        result => {
          if (result.status === "failure") {
            this.openNotification("error", result.data);
          } else {
            this.setState({ stockProds: result.data });
          }
        },
        error => {
          this.setState({
            isLoaded: true,
            error
          });
        }
      );
  };

  handleProdChange = id => {
    this.setState({ stockProductId: id });
  };

  addRecipe = () => {
    let hdr = localStorage.token;
    const { productId, stockProductId, quantity } = this.state;
    let data = JSON.stringify({ stockProductId, quantity });
    console.log(data);
    fetch(`${endpoint}/recipe/${productId}`, {
      method: "PUT",
      body: data,
      headers: {
        "x-auth-token": hdr,
        "Content-Type": "application/json"
      }
    })
      .then(res => res.json())
      .then(
        result => {
          if (result.status === "failure") {
            this.openNotification("error", result.data);
          } else {
            this.openNotification("success", result.message);
            this.setState({
              editingRecipe: false,
              editingRecipeIndex: -1,
              visible: false,
              stockProductId: ""
            });
          }
        },
        error => {
          this.setState({
            isLoaded: true,
            error
          });
        }
      );
  };

  deleteRecipe = recipeId => {
    let hdr = localStorage.token;
    console.log(recipeId);
    fetch(`${endpoint}/recipe/${recipeId}`, {
      method: "DELETE",
      headers: {
        "x-auth-token": hdr
      }
    })
      .then(res => res.json())
      .then(
        result => {
          if (result.status === "failure") {
            this.openNotification("error", result.data);
          } else {
            this.openNotification("success", result.message);
            this.setState({
              editingRecipe: false,
              editingRecipeIndex: -1,
              visible: false
              // stockProductId: ''
            });
          }
        },
        error => {
          this.setState({
            isLoaded: true,
            error
          });
        }
      );
  };

  handleQuantityChange = e => {
    this.setState({ quantity: e.target.value });
  };

  render() {
    console.log(this.state);
    const {
      isLoaded,
      error,
      category,
      productName,
      recipes,
      quantity,
      editingRecipe,
      editingRecipeIndex
    } = this.state;
    const components = {
      body: {
        row: EditableFormRow,
        cell: EditableCell
      }
    };
    const types = [
      `<Option key="PLU1">PLU1</Option>`,
      `<Option key="PLU2">PLU2</Option>`
    ];

    const categoryColumns = this.categoryColumns.map(col => {
      if (!col.editable) {
        return col;
      }
      return {
        ...col,
        onCell: record => ({
          record,
          inputType: col.dataIndex === "price" ? "number" : "text",
          dataIndex: col.dataIndex,
          title: col.title,
          editing: this.isEditing(record)
        })
      };
    });

    const productColumns = this.productColumns.map(col => {
      if (!col.editable) {
        return col;
      }
      return {
        ...col,
        onCell: record => ({
          record,
          inputType: col.dataIndex === "price" ? "number" : "text",
          dataIndex: col.dataIndex,
          title: col.title,
          editing: this.isEditing(record)
        })
      };
    });
    // if (!isLoaded) {
    //   return (
    //     <Home>
    //       <Loader />
    //     </Home>
    //   );
    // } else
    if (error) {
      return <div>Error: {error.message}</div>;
    } else {
      return (
        <Home isAdmin={true}>
          <div className="report-container">
            <span className="item">
              Manage {category ? "Categories" : "Products"}
            </span>
            <span className="item">
              <span id="less-visible">
                {!this.state.category ? (
                  <div onClick={this.goToCat} className="back-button-border">
                    <i className="fa fa-arrow-circle-left" aria-hidden="true" />
                    <span>Back</span>
                  </div>
                ) : (
                  ""
                )}
              </span>
            </span>
          </div>

          {category ? (
            <Fragment>
              <Form layout="inline" onSubmit={this.handleAddCategory}>
                <FormItem>
                  <Input
                    style={{ width: 200, marginRight: "10px" }}
                    compact={true}
                    onChange={e => this.setState({ newItem: e.target.value })}
                    placeholder="Category Name"
                    value={this.state.newItem}
                    required
                  />
                </FormItem>
                <FormItem>
                  <Select
                    mode="tags"
                    style={{ width: 180 }}
                    placeholder="Choose Type"
                    onChange={this.handleChange}
                  >
                    <Option value="PLU1" key="PLU1">
                      PLU1
                    </Option>
                    <Option value="PLU2" key="PLU2">
                      PLU2
                    </Option>
                  </Select>
                </FormItem>
                <FormItem>
                  <Button
                    type="primary"
                    htmlType="submit"
                    style={{ marginBottom: 16 }}
                  >
                    Add a Category
                  </Button>
                </FormItem>
              </Form>
              <Table
                rowKey={record => record.name}
                loading={isLoaded ? false : true}
                onChange={pagination => this.pageChange(pagination)}
                components={components}
                bordered
                dataSource={this.state.items}
                columns={categoryColumns}
                rowClassName="editable-row"
              />
              <Modal
                title="Choose Icon"
                visible={this.state.iconModalVisible}
                footer={null}
                width={"80%"}
                onCancel={this.handleIconModalCancel}
              >
                {this.state.icons.map((icon, index) => {
                  return (
                    <span key={icon._id}>
                      <img
                        src={`${endpoint}/${icon.path}`}
                        className="choose-icon"
                        onClick={() => this.selectIcon(icon._id)}
                      />
                    </span>
                  );
                })}
              </Modal>
            </Fragment>
          ) : (
            <Fragment>
              <Form layout="inline" onSubmit={this.handleAddProduct}>
                <FormItem>
                  <Input
                    style={{ width: 200, marginRight: "10px" }}
                    compact={true}
                    onChange={e => this.setState({ newItem: e.target.value })}
                    placeholder="Product Name"
                    value={this.state.newItem}
                    required
                  />
                </FormItem>
                <FormItem>
                  <Input
                    type="number"
                    prefix="Kr."
                    style={{ width: 100, marginRight: "10px" }}
                    compact={true}
                    onChange={e => this.setState({ newPrice: e.target.value })}
                    placeholder="Price"
                    min="0"
                    value={this.state.newPrice}
                    required
                  />
                </FormItem>
                <FormItem>
                  <Input
                    type="number"
                    style={{ width: 100, marginRight: "10px" }}
                    compact={true}
                    onChange={e => this.setState({ itemNo: e.target.value })}
                    placeholder="No."
                    min="0"
                    value={this.state.itemNo}
                    required
                  />
                </FormItem>
                <FormItem>
                  <Button
                    type="primary"
                    htmlType="submit"
                    style={{ marginBottom: 16 }}
                  >
                    Add a Product
                  </Button>
                </FormItem>
              </Form>
              <Table
                rowKey={record => record.name}
                loading={isLoaded ? false : true}
                onChange={pagination => this.pageChange(pagination)}
                components={components}
                bordered
                dataSource={this.state.items}
                columns={productColumns}
                rowClassName="editable-row"
              />
              <Modal
                title={`${productName} Recipe`}
                visible={this.state.visible}
                onOk={this.handleOk}
                onCancel={this.handleCancel}
                footer={null}
                width={640}
              >
                <Form onSubmit={e => this.handleSubmit(e)}>
                  {/* <Row type="flex" justify="space-between">
                    <Col span={6}>
                      <FormItem>
                        <Select
                          required
                          showSearch
                          onChange={this.handleRecipeChange} Comment this
                          placeholder="Stock Category"
                          optionFilterProp="children"
                        >
                          {stockCats
                            ? stockCats.map(item => {
                                return (
                                  <Option
                                    value={item._id}
                                    name={item.name}
                                    onClick={() =>
                                      this.handleCatChange(item._id)
                                    }
                                  >
                                    {item.name}
                                  </Option>
                                );
                              })
                            : ''}
                        </Select>
                      </FormItem>
                    </Col>
                    <Col span={1} />
                    <Col span={6}>
                      <FormItem>
                        <Select
                          required
                          showSearch
                          onChange={this.handleRecipeChange} Comment this
                          placeholder="Stock Sub-Category"
                          optionFilterProp="children"
                        >
                          {stockProds.map(item => {
                            return (
                              <Option
                                value={item._id}
                                name={item.name}
                                onClick={() => this.handleProdChange(item._id)}
                              >
                                {item.name}
                              </Option>
                            );
                          })}
                        </Select>
                      </FormItem>
                    </Col>
                    <Col span={1} />
                    <Col span={4}>
                      <FormItem>
                        <Input
                          type="number"
                          placeholder="quantity"
                          value={quantity}
                          onChange={this.handleQuantityChange}
                        />
                      </FormItem>
                    </Col>
                    <Col span={1} />
                    <Col span={4}>
                      <FormItem>
                        <Button
                          type="dashed"
                          style={{ width: '100%' }}
                          onClick={this.addRecipe}
                        >
                          Add
                        </Button>
                      </FormItem>
                    </Col>
                    <Col span={6} />
                  </Row> */}
                  <Row type="flex" justify="space-between">
                    <Col span={2} />
                    <Col span={20}>
                      <table>
                        <tr>
                          <th style={{ padding: "5px 15px" }}>Name</th>
                          <th style={{ padding: "5px 15px" }}>Quantity</th>
                        </tr>
                        {recipes.map((item, index) => {
                          return (
                            <Fragment>
                              {editingRecipe && index === editingRecipeIndex ? (
                                <tr>
                                  <td>{item.stockProduct.name}</td>
                                  <td>
                                    <Input
                                      value={quantity}
                                      onChange={this.handleRecipeChange}
                                      name="quantity"
                                      type="number"
                                    />
                                  </td>
                                  <td>
                                    <Button onClick={this.addRecipe}>
                                      Save
                                    </Button>
                                  </td>
                                  <td>
                                    <Button
                                      type="danger"
                                      onClick={() =>
                                        this.deleteRecipe(item._id)
                                      }
                                    >
                                      Delete
                                    </Button>
                                  </td>
                                </tr>
                              ) : (
                                <tr>
                                  <td>{item.stockProduct.name}</td>
                                  <td>{item.quantity}</td>
                                  <td>
                                    <Button
                                      onClick={() =>
                                        this.handleEdit(
                                          index,
                                          item.stockProduct.name,
                                          item.quantity,
                                          item.stockProduct._id
                                        )
                                      }
                                    >
                                      Edit
                                    </Button>
                                  </td>
                                  <td>
                                    <Button
                                      type="danger"
                                      onClick={() =>
                                        this.deleteRecipe(item._id)
                                      }
                                    >
                                      Delete
                                    </Button>
                                  </td>
                                </tr>
                              )}
                            </Fragment>
                          );
                        })}
                      </table>
                    </Col>
                    <Col span={2} />
                  </Row>
                </Form>
              </Modal>
            </Fragment>
          )}
        </Home>
      );
    }
  }
}

export default EditableTable;
