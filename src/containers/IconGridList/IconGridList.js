import React, { Component } from 'react';
import IconGridItem from '../IconGridItem/IconGridItem';
import endpoint from '../../helpers/endpoint';
import { Col } from 'antd';

const bgColors = [
  '#359aff',
  '#3caa3c',
  '#20b8bd',
  '#bb214b',
  '#19467f',
  '#dcb016',
  '#696969',
  '#9e82ed',
  '#bb214b',
  '#7cde97',
  '#282928',
  '#f2bb15'
];

class IconGridList extends Component {
  render() {
    console.log('props', this.props);
    // if (!this.props.products) {
    //   var product = [];
    // } else {
    //   var product = this.props.products;
    // }
    // console.log(this.props.products);
    if (this.props.products.length < 1) {
      return <React.Fragment />;
    } else {
      const iconGridList = this.props.products.map((product, index) => {
        // productName: this.props.products.productName
        // console.log(product);

        // if (product.catType && product.catType.type === true)

        if (this.props.isBilling === false) {
          console.log('test', product.catType);
          return (
            <Col
              className="gutter-row"
              span={6}
              style={
                this.props.category
                  ? { marginBottom: '4.5em' }
                  : { marginBottom: '4.5em' }
              }
            >
              <IconGridItem
                color={bgColors[index % bgColors.length]}
                category={this.props.category}
                productName={product.name}
                productPrice={product.price}
                icon={`${endpoint}/${product.image}`}
                id={product._id}
                clicked={productInfo => this.props.handleClick(productInfo)}
              />
            </Col>
          );
        } else {
          const type = this.props.type;
          if (product.catType) {
            console.log('prod', product.catType[type]);
            console.log('type', type);
            console.log(type == product.catType);
          }
          if (product.catType && product.catType[type] == true) {
            console.log('work');
            return (
              <Col
                className="gutter-row"
                span={6}
                style={
                  this.props.category
                    ? { marginBottom: '4.5em' }
                    : { marginBottom: '4.5em' }
                }
              >
                <IconGridItem
                  color={bgColors[index % bgColors.length]}
                  category={this.props.category}
                  productName={product.name}
                  productPrice={product.price}
                  icon={`${endpoint}/${product.image}`}
                  id={product._id}
                  clicked={productInfo => this.props.handleClick(productInfo)}
                />
              </Col>
            );
          }
        }
      });
      return iconGridList;
    }
  }
}

export default IconGridList;
