import React, { Component } from 'react';
import Home from '../Home/Home';
import { Button, Col, Row, notification } from 'antd';
import './PLU.css';
import { connect } from 'react-redux';
import { push } from 'react-router-redux';
import eatDrink from '../../assets/eatdrink.png';
import steering from '../../assets/steering.png';
import wrapper from '../../assets/wrapper.png';
import endpoint from '../../helpers/endpoint';

class PLUComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      error: false,
      isLoaded: false
    };

    this.addTableTakeAway = this.addTableTakeAway.bind(this);
  }

  openNotification = (type, message) => {
    notification[type]({
      message: message
    });
  };

  createOrder = str => {
    this.setState({ orderType: str }, function() {
      this.checkForDriveOut();
    });
  };

  checkForDriveOut = () => {
    if (this.state.orderType === 'Drive Out') {
      this.getResDetails();
    } else {
      this.addTableTakeAway();
    }
  };

  getResDetails = () => {
    var hdr = localStorage.token;
    fetch(`${endpoint}/profile`, {
      method: 'GET',
      headers: {
        'x-auth-token': hdr
      }
    })
      .then(res => res.json())
      .then(
        result => {
          console.log(result);
          if (result.status === 'failure') {
            this.openNotification('error', result.data);
          } else {
            console.log('dataaa', result.data);
            this.addTableTakeAway('Drive Out', result.data.driveoutCharge);
          }
        },
        // Note: it's important to handle errors here
        // instead of a catch() block so that we don't swallow
        // exceptions from actual bugs in components.
        error => {
          this.setState({
            isLoaded: true,
            error
          });
        }
      );
  };

  addTableTakeAway = (type, charge) => {
    var hdr = localStorage.token;
    // let fullDate = new Date();
    // let date = fullDate.getDate();
    // let month = fullDate.getMonth();
    // let year = fullDate.getFullYear();

    var data;

    if (type === 'Drive Out') {
      data = JSON.stringify({
        number: 0,
        orderType: this.state.orderType,
        driveoutCharge: charge /*created: unix time string*/
      });
    } else {
      data = JSON.stringify({
        number: 0,
        orderType: this.state.orderType,
        driveoutCharge: 0 /*created: unix time string*/
      });
    }

    console.log('dataa', data);
    fetch(`${endpoint}/table`, {
      method: 'POST',
      body: data,
      headers: {
        'x-auth-token': hdr,
        'Content-Type': 'application/json'
      }
    })
      .then(res => res.json())
      .then(
        result => {
          if (result.status === 'failure') {
            this.openNotification('error', result.data);
          } else {
            this.openNotification('success', result.message);
            console.log(result);
            localStorage.setItem('table', result.data._id);
            this.props.history.push('/billing');
          }
        },
        // Note: it's important to handle errors here
        // instead of a catch() block so that we don't swallow
        // exceptions from actual bugs in components.
        error => {
          this.setState({
            isLoaded: true,
            error
          });
        }
      );
  };

  render() {
    return (
      <Home>
        <div className="PLU-wrapper test1">
          <Row className="plu-flex" style={{ textAlign: 'center' }}>
            <Col>
              <Button
                onClick={() => {
                  this.navigate('/tableview');
                }}
                type="primary"
                className="home-button-style home-button-color test3"
              >
                <img
                  alt="eat drink"
                  src={eatDrink}
                  className="eat-drink-logo"
                />
                <span>EAT/ DRINK HERE</span>
                <span>PLU 1</span>
              </Button>
            </Col>
            <Col>
              <Button
                type="primary"
                className="home-button-style home-button-color-2 test3"
                onClick={() => {
                  this.createOrder('Take away');
                }}
              >
                <img alt="eat drink" src={wrapper} className="eat-drink-logo" />
                <span>TAKE OUT</span>
                <span>PLU 2</span>
              </Button>
            </Col>
          </Row>

          <Row style={{ marginTop: 40 }}>
            <Col className="PLU-submit">
              <div style={{ textAlign: 'center' }}>
                <Button
                  shape="circle"
                  className="home-button-driveout"
                  onClick={() => {
                    this.createOrder('Drive Out');
                  }}
                >
                  <img src={steering} className="white-logo" />
                  <span>Drive Out</span>
                  <span>PLU 2</span>
                </Button>
              </div>
            </Col>
          </Row>
        </div>
      </Home>
    );
  }
  navigate = route => {
    const { dispatch } = this.props;
    dispatch(push(route));
  };
}
const mapStateToProps = state => {
  return {};
};
const PLU = connect(mapStateToProps)(PLUComponent);
export default PLU;
