import React from 'react';
import { Button, Col } from 'antd';

var IconGrid = props => {
  let bgColors = [
    '#359aff',
    '#3caa3c',
    '#20b8bd',
    '#bb214b',
    '#19467f',
    '#dcb016',
    '#696969',
    '#dcb016'
  ];
  var number = Math.floor(Math.random() * bgColors.length);
  var styles = {
    backgroundColor: `${bgColors[number]} !important`
  };

  return (
    <Col className="items gutter-row">
      <div className="button-style">
        <Button
          className="drinks button-size-icon"
          id={props.categoryID}
          onClick={props.clicked}
          style={styles}
        >
          <div className="menu-options white">
            <img src={props.drinkIcon} className="product-image" />
            <span>{props.productName}</span>
            <span>{props.productPrice}</span>
          </div>
        </Button>
      </div>
    </Col>
  );
};

export default IconGrid;
