import React, { Component } from "react";
import { connect } from "react-redux";
import Home from "../Home/Home";
import { Row, notification } from "antd";
import "./Dashboard.css";
import Loader from "../Loader/Loader";
import PropTypes from "prop-types";
import IconGridList from "../IconGridList/IconGridList";
import { push } from "react-router-redux";
import endpoint from "../../helpers/endpoint";

class DashboardComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      error: false,
      isLoaded: false,
      modalIsOpen: false,
      name: "",
      category: true,
      catId: "",
      products: [],
      price: 0,
      searchText: ""
    };

    // this.openModal = this.openModal.bind(this);
    // this.closeModal = this.closeModal.bind(this);
  }

  // openModal() {
  //   this.setState({ modalIsOpen: true });
  // }

  // closeModal() {
  //   this.setState({ modalIsOpen: false });
  // }

  static propTypes = {
    isBilling: PropTypes.bool
  };

  ggwp() {
    console.log("ww");
  }

  handleSearchChange = e => {
    this.setState({ searchText: e.target.value });
  };

  // handleChange = e => {
  //   this.setState({ [e.target.name]: e.target.value });
  // };

  handleClick = category => {
    if (category) {
      this.setState({ isLoaded: false });
      console.log(category.id);
      this.productPage(category.id);
    } else {
      console.log("beep bop");
    }
  };

  // handleSubmit = e => {
  //   e.preventDefault();
  //   var hdr = localStorage.token;
  //   var data = JSON.stringify({ name: this.state.name });
  //   fetch(`${endpoint}/category`, {
  //     method: 'POST',
  //     body: data,
  //     headers: {
  //       'x-auth-token': hdr,
  //       'Content-Type': 'application/json'
  //     }
  //   })
  //     .then(res => res.json())
  //     .then(
  //       result => {
  //         if (result.status === 'failure') {
  //           alert(result.data);
  //         } else {
  //           this.props.history.push('/');
  //         }
  //       },
  //       // Note: it's important to handle errors here
  //       // instead of a catch() block so that we don't swallow
  //       // exceptions from actual bugs in components.
  //       error => {
  //         this.setState({
  //           isLoaded: true,
  //           error
  //         });
  //       }
  //     );
  // };

  productPage = id => {
    var hdr = localStorage.token;
    fetch(`${endpoint}/product/${id}`, {
      method: "GET",
      headers: {
        "x-auth-token": hdr
      }
    })
      .then(res => res.json())
      .then(
        result => {
          if (result.status === "failure") {
            this.openNotification("error", result.data);
          } else {
            this.setState({
              isLoaded: true,
              products: result.data,
              category: false,
              catId: id
            });
          }
        },
        // Note: it's important to handle errors here
        // instead of a catch() block so that we don't swallow
        // exceptions from actual bugs in components.
        error => {
          this.setState({
            isLoaded: true,
            error
          });
        }
      );
  };

  openNotification = (type, message) => {
    notification[type]({
      message: message
    });
  };

  // addProductCat = e => {
  //   e.preventDefault();
  //   var hdr = localStorage.token;
  //   var data = JSON.stringify({
  //     name: this.state.name,
  //     price: this.state.price
  //   });
  //   fetch(`${endpoint}/product/${this.state.catId}`, {
  //     method: 'POST',
  //     body: data,
  //     headers: {
  //       'x-auth-token': hdr,
  //       'Content-Type': 'application/json'
  //     }
  //   })
  //     .then(res => res.json())
  //     .then(
  //       result => {
  //         if (result.status === 'failure') {
  //           alert('Check your credentials and then fill the form.');
  //         } else {
  //           this.setState({
  //             isLoaded: true,
  //             products: result.data,
  //             category: true,
  //             modalIsOpen: false
  //           });
  //         }
  //       },
  //       // Note: it's important to handle errors here
  //       // instead of a catch() block so that we don't swallow
  //       // exceptions from actual bugs in components.
  //       error => {
  //         this.setState({
  //           isLoaded: true,
  //           error
  //         });
  //       }
  //     );
  // };

  goToCat = () => {
    this.setState({ category: true });
  };

  // shouldComponentUpdate() {
  //   return true;
  // }

  componentDidMount() {
    var hdr = localStorage.token;
    fetch(`${endpoint}/category`, {
      method: "GET",
      headers: {
        "x-auth-token": hdr
      }
    })
      .then(res => res.json())
      .then(
        result => {
          console.log(result.status);
          if (result.status === "failure") {
            localStorage.clear();
            this.navigate("/");
          } else {
            this.setState({
              isLoaded: true,
              cats: result.data
            });
          }
        },
        // Note: it's important to handle errors here
        // instead of a catch() block so that we don't swallow
        // exceptions from actual bugs in components.
        error => {
          this.setState({
            isLoaded: true,
            error
          });
        }
      );
  }

  render() {
    console.log("homestate:", this.state);
    const { error, isLoaded } = this.state;
    if (!isLoaded) {
      return (
        <Home>
          <Loader />
        </Home>
      );
    } else if (error) {
      return <div>Error: {error.message}</div>;
    } else {
      return (
        <Home
          searchText={this.state.searchText}
          handleSearchChange={e => this.handleSearchChange(e)}
        >
          <div
            className={
              {
                /* this.props.isBilling
                ? 'dashboard-billing'
                : 'dashboard-main-wrapper' */
              }
            }
          >
            {!this.state.category ? (
              <div onClick={this.goToCat} className="back-button">
                <i class="fa fa-arrow-circle-left" aria-hidden="true" />
                <span>Back</span>
              </div>
            ) : (
              ""
            )}
            <Row
              className="dashboard-flex"
              type="flex"
              align="middle"
              gutter={0}
            >
              {this.state.category && this.state.cats.length > 0 ? (
                <IconGridList
                  isBilling={false}
                  products={this.state.cats}
                  category={this.state.category}
                  handleClick={categories => this.handleClick(categories)}
                />
              ) : (
                <IconGridList
                  isBilling={false}
                  products={this.state.products}
                  category={this.state.category}
                  handleClick={() => {
                    return null;
                  }}
                />
              )}
            </Row>
          </div>
        </Home>
      );
    }
  }
  navigate = route => {
    const { dispatch } = this.props;
    dispatch(push(route));
  };
}
const mapStateToProps = state => {
  return {};
};
const Dashboard = connect(mapStateToProps)(DashboardComponent);
export default Dashboard;
