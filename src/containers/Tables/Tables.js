import React, { Component, Fragment } from 'react';
import { connect } from 'react-redux';
import Home from '../Home/Home';
import { Button, Col, Row, notification } from 'antd';
import './Tables.css';
import PropTypes from 'prop-types';
import endpoint from '../../helpers/endpoint';
import Loader from '../Loader/Loader';

class TablesComponent extends Component {
  static propTypes = {
    isBilling: PropTypes.bool
  };

  constructor(props) {
    super(props);
    this.state = {
      error: false,
      isLoaded: false,
      activeTable: [],
      noTable: [],
      driveOut: [],
      takeAway: []
    };
  }

  showTable = id => {
    localStorage.setItem('table', id);
    this.props.history.push('/billing');
  };

  componentWillMount() {
    var hdr = localStorage.token;
    fetch(`${endpoint}/table`, {
      method: 'GET',
      headers: {
        'x-auth-token': hdr
      }
    })
      .then(res => res.json())
      .then(
        result => {
          if (result.status === 'failure') {
            this.openNotification('error', result.data);
          } else {
            this.setState({
              isLoaded: true,
              activeTable: result.data.activeTable,
              noTable: result.data.noTable,
              driveOut: result.data.driveOut,
              takeAway: result.data.takeAway
            });
            // console.log(this.state);
          }
        },
        // Note: it's important to handle errors here
        // instead of a catch() block so that we don't swallow
        // exceptions from actual bugs in components.
        error => {
          this.setState({
            isLoaded: true,
            error
          });
        }
      );
  }

  openNotification = (type, message) => {
    notification[type]({
      message: message
    });
  };

  render() {
    console.log(this.state);
    const {
      error,
      isLoaded,
      activeTable,
      driveOut,
      takeAway,
      noTable
    } = this.state;
    // console.log(items);
    if (!isLoaded) {
      return (
        <Home>
          <Loader />
        </Home>
      );
    } else if (error) {
      return <div>Error: {error.message}</div>;
    } else if (isLoaded) {
      // console.log(items);
      return (
        <Home>
          <div
            className={
              this.props.isBilling
                ? 'dashboard-billing'
                : 'dashboard-main-wrapper'
            }
          >
            <Row className="dashboard-flex">
              {activeTable.length > 0 ? (
                <h3 style={{ width: '100%' }}>Active Tables</h3>
              ) : (
                ''
              )}
              {activeTable.length > 0
                ? activeTable.map((item, index) => {
                    return (
                      <Fragment>
                        <Col
                          onClick={() => {
                            this.showTable(item._id);
                          }}
                          key={index}
                          className="items gutter-row"
                        >
                          <div className="button-style">
                            <Button
                              className="drinks button-size-icon"
                              shape="circle"
                            >
                              <div className="menu-options white">
                                {item.orderType === 'PLU1' &&
                                item.number > 0 ? (
                                  <span>Table {item.number}</span>
                                ) : (
                                  <span>
                                    {item.orderType} {item.number}
                                  </span>
                                )}
                              </div>
                            </Button>
                          </div>
                        </Col>
                      </Fragment>
                    );
                  })
                : ''}
              {takeAway.length > 0 ? (
                <h3 style={{ width: '100%' }}>Take Aways</h3>
              ) : (
                ''
              )}
              {takeAway.length > 0
                ? takeAway.map((item, index) => {
                    return (
                      <Fragment>
                        <Col
                          onClick={() => {
                            this.showTable(item._id);
                          }}
                          key={index}
                          className="items gutter-row"
                        >
                          <div className="button-style">
                            <Button
                              className="drinks button-size-icon"
                              shape="circle"
                            >
                              <div className="menu-options white">
                                {item.orderType === 'PLU1' &&
                                item.number > 0 ? (
                                  <span>Table {item.number}</span>
                                ) : (
                                  <span>{item.orderType}</span>
                                )}
                              </div>
                            </Button>
                          </div>
                        </Col>
                      </Fragment>
                    );
                  })
                : ''}

              {driveOut.length > 0 ? (
                <h3 style={{ width: '100%' }}>Drive Outs</h3>
              ) : (
                ''
              )}
              {driveOut.length > 0
                ? driveOut.map((item, index) => {
                    return (
                      <Fragment>
                        <Col
                          onClick={() => {
                            this.showTable(item._id);
                          }}
                          key={index}
                          className="items gutter-row"
                        >
                          <div className="button-style">
                            <Button
                              className="drinks button-size-icon"
                              shape="circle"
                            >
                              <div className="menu-options white">
                                {item.orderType === 'PLU1' &&
                                item.number > 0 ? (
                                  <span>Table {item.number}</span>
                                ) : (
                                  <span>{item.orderType}</span>
                                )}
                              </div>
                            </Button>
                          </div>
                        </Col>
                      </Fragment>
                    );
                  })
                : ''}

              {noTable.length > 0 ? (
                <h3 style={{ width: '100%' }}>No Table</h3>
              ) : (
                ''
              )}
              {noTable.length > 0
                ? noTable.map((item, index) => {
                    return (
                      <Fragment>
                        <Col
                          onClick={() => {
                            this.showTable(item._id);
                          }}
                          key={index}
                          className="items gutter-row"
                        >
                          <div className="button-style">
                            <Button
                              className="drinks button-size-icon"
                              shape="circle"
                            >
                              <div className="menu-options white">
                                {item.orderType === 'PLU1' &&
                                item.number > 0 ? (
                                  <span>Table {item.number}</span>
                                ) : (
                                  <span>{item.orderType}</span>
                                )}
                              </div>
                            </Button>
                          </div>
                        </Col>
                      </Fragment>
                    );
                  })
                : ''}
            </Row>
          </div>
        </Home>
      );
    } else {
      return <Home />;
    }
  }
}
const mapStateToProps = state => {
  return {};
};
const Tables = connect(mapStateToProps)(TablesComponent);
export default Tables;
