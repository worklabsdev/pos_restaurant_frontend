import React, { Component } from 'react';
import { Route } from 'react-router-dom';
import { Router } from 'react-router';
import Home from './containers/Home/Home';
import Dashboard from './containers/Dashboard/Dashboard';
import AdminDashboard from './containers/AdminDashboard/AdminDashboard';
import PLU from './containers/PLU/PLU';
import TableView from './containers/TableView/TableView';
import Reports from './containers/Reports/Reports';
import Billing from './containers/Billing/Billing';
import LandingPage from './containers/LandingPage/LandingPage';
import Stocks from './containers/Stock/stock.js';
import CRM from './containers/crm/crm.js';
import Invoice from './containers/invoice/invoice.js';
import taccountant from './containers/taccountant/taccountant.js';
import ikAccounts from './containers/ik-accounts/ik-accounts.js';
import details from './containers/detailedReports/detailedReports.js';
import resProfile from './containers/restaurantProfile/restaurantProfile.js';
import StockDetails from './containers/StockDetails/StockDetails.js';
import changeStock from './containers/stockChange/stockChange.js';
import HRM from './containers/hrm/hrm.js';
import emptab from './containers/EmployeeTable/emptable.js';
import Tables from './containers/Tables/Tables.js';
import CTable from './containers/CTable/CTable.js';
import Manage from './containers/Manage/Manage.js';
import SingleInvoice from './containers/SingleInvoice/SingleInvoice.js';
import UnpaidInvoice from './containers/UnpaidInvoice/UnpaidInvoice.js';
import Setting from './containers/Setting/Setting.js';

export default class Main extends Component {
  render() {
    return (
      <Router history={this.props.history}>
        <div>
          <Route path="/stockChange" exact component={changeStock} />
          <Route path="/stockDetails" exact component={StockDetails} />
          <Route path="/profile" exact component={resProfile} />
          <Route path="/details" exact component={details} />
          <Route path="/ikaccounts" exact component={ikAccounts} />
          <Route path="/taccountant" exact component={taccountant} />
          <Route path="/crm" exact component={CRM} />
          <Route path="/invoice" exact component={Invoice} />
          <Route path="/stock" exact component={Stocks} />
          <Route path="/home" component={Home} />
          <Route path="/dashboard" component={Dashboard} />
          <Route path="/plu" component={PLU} />
          <Route path="/tableview" component={TableView} />
          <Route path="/reports" component={Reports} />
          <Route path="/billing" component={Billing} />
          <Route path="/admin" component={AdminDashboard} />
          <Route path="/" exact component={LandingPage} />
          <Route path="/hrm" exact component={HRM} />
          <Route path="/emptable" exact component={emptab} />
          <Route path="/tables" exact component={Tables} />
          <Route path="/ctable" exact component={CTable} />
          <Route path="/manage" exact component={Manage} />
          <Route path="/singleinvoice" exact component={SingleInvoice} />
          <Route path="/unpaid" exact component={UnpaidInvoice} />
          <Route path="/setting" exact component={Setting} />
        </div>
      </Router>
    );
  }
}
