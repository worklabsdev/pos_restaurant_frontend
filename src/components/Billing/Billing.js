import React, {Component} from 'react';
import { push } from 'react-router-redux';
import './Billing.css'
export default class RightMenu extends Component {
  render(){
    return(
    	<div>
    		<div className="Billing-header">
    		  <span>Table number</span>
    		  <span>5/ID</span>
    		</div>
        <div>
          <ul>
            <li><span>sometext</span><span>some</span></li>
            <li><span>sometext</span><span>some</span></li>
            <li><span>sometext</span><span>some</span></li>
          </ul>
          <ul>
            <li><span>Discount</span><span>some</span></li>
            <li><span>VAT</span><span>some</span></li>
            <li><span>Total</span><span>some</span></li>
          </ul>
        </div>
        <input type="submit" value="Print" className="billing-button-yellw"/>
        <button value="Send" />
    	</div>
    )
  }

navigate = (route) => {
const {dispatch} = this.props;
dispatch(push(route))
  }
}