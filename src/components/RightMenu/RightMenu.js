import React, { Component } from "react";
import { push } from "react-router-redux";
import { Card, Form, Modal, Button, Input } from "antd";
import "./RightMenu.css";
import returnItem from "../../assets/return.png";
import payment from "../../assets/payment.png";
import FormItem from "antd/lib/form/FormItem";

const confirm = Modal.confirm;

export default class RightMenu extends Component {
  constructor(props) {
    super(props);

    this.state = { visible: false, value: 0, type: "", isCredit: false };
  }

  showModal = () => {
    this.setState({
      visible: true,
      type: "percent"
    });
  };

  showConfirm = () => {
    confirm({
      title: "Do you Want to mark this as credit invoice ?",
      onOk: () => {
        this.setState({ isCredit: true });
      },
      okText: "Yes",
      cancelText: "No",
      onCancel: () => {
        console.log("Cancel");
      }
    });
  };

  handleChange = e => {
    this.setState({ value: e.target.value });
  };

  handleClose = e => {
    console.log(e);
    this.setState({
      visible: false
    });
  };

  onChange = e => {
    console.log("radio checked", e.target.value);
    this.setState({
      type: e.target.value
    });
  };
  render() {
    const { type, value } = this.state;
    return (
      <div id="right-menu-main-wrapper" style={{ marginTop: "-10px" }}>
        <Card className="card-style">
          <div className="right-upper-button">
            <button className="circle-button1 shadow circle">
              <p>Name</p>
            </button>
            <button
              onClick={() => {
                this.navigate("/reports");
              }}
              className="circle-button2 circle shadow "
            >
              <img src={returnItem} className="return-image-right" />
              <p>RAP</p>
            </button>
          </div>

          <div className="right-upper-button">
            <button className="circle-button3 circle  shadow ">
              <img src={payment} className="return-image-right" />
              <p>Payment</p>
            </button>
            <button className="circle-button4 circle shadow">
              <img src={returnItem} className="return-image-right-black" />
              <p>Return</p>
            </button>
          </div>

          <div className="right-upper-button">
            <Button className="leftButton shadow">
              <b>Apple pay</b>
            </Button>
            <Button
              className="rightButton shadow"
              onClick={() => this.props.cashGiven(100)}
            >
              <b>100</b>
            </Button>
          </div>
          <div className="right-upper-button">
            <Button className="leftButton shadow">
              <b>Bank</b>
            </Button>
            <Button
              className="rightButton shadow"
              onClick={() => this.props.cashGiven(200)}
            >
              <b>200</b>
            </Button>
          </div>

          <div className="right-upper-button">
            <Button className="leftButton shadow">
              <b>Cash</b>
            </Button>
            <Button
              className="rightButton shadow"
              onClick={() => this.props.cashGiven(500)}
            >
              <b>500</b>
            </Button>
          </div>

          <div className="right-upper-button">
            <Button className="leftButton shadow">
              <b>Cash Box</b>
            </Button>
            <Button
              className="rightButton shadow"
              onClick={() => this.props.cashGiven(1000)}
            >
              <b>1000</b>
            </Button>
          </div>
          <div className="right-upper-button">
            <Button className="leftButton shadow" onClick={this.showModal}>
              <b>Discount</b>
            </Button>
            <Button
              className="rightButton shadow"
              onClick={() => {
                this.showConfirm();
                this.props.clickedCredit();
              }}
            >
              <b>Credit</b>
            </Button>
            <Modal
              width={400}
              title="Discount"
              visible={this.state.visible}
              onOk={() => {
                this.props.handleOk(type, value);
                this.handleClose();
              }}
              onCancel={this.handleClose}
            >
              <Form>
                <FormItem
                  style={{
                    display: "flex",
                    "justify-content": "center"
                  }}
                >
                  {/* <RadioGroup
                    name="radiogroup"
                    onChange={this.onChange}
                    value={this.state.type}
                  >
                    <Radio value={'percent'}>Percentage</Radio>
                    <Radio value={'amount'}>Flat</Radio>
                  </RadioGroup> */}
                </FormItem>
                <FormItem
                  style={{
                    display: "flex",
                    "justify-content": "center"
                  }}
                >
                  <Input
                    type="number"
                    min="1"
                    prefix={this.state.type === "percent" ? "%" : "Kr."}
                    style={{ "max-width": 200 }}
                    value={this.state.value}
                    onChange={e => this.handleChange(e)}
                  />
                </FormItem>
              </Form>
            </Modal>
          </div>
        </Card>
      </div>
    );
  }

  navigate = route => {
    const { dispatch } = this.props;
    // dispatch(push(route));
  };
}
